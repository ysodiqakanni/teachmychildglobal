﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class UserAccountDao : CoreDao<ApplicationUser>, IUserAccountDao
    {
        public UserAccountDao(DbContext _dbContext) : base(_dbContext)
        {
            
        }
    }
}
