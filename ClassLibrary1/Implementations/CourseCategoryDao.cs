﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class CourseCategoryDao : CoreDao<CourseCategory>, ICourseCategoryDao
    {
        public CourseCategoryDao(DbContext _dbContext) : base(_dbContext)
        {
                
        }
    }
}
