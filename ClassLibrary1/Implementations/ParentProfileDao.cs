﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class ParentProfileDao : CoreDao<ParentProfile>, IParentProfileDao
    {
        public ParentProfileDao(DbContext context) : base(context)
        {
          
        }
        public ParentProfile GetByUserAccountId(string id)
        {
            var profile = Find(p => p.Account.Id == id).SingleOrDefault();
            return profile;
        }

        public ParentProfile GetByUserAccountName(string username)
        {
            var profile = Find(p => String.Compare(p.Account.UserName, username, true) ==0).SingleOrDefault();
            return profile;
        }
    }
}
