﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class MessageDao : CoreDao<Message>, IMessageDao
    {
        public MessageDao(DbContext ctx)  : base(ctx)
        {

        }
    }
}
