﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class ChatDao : CoreDao<Chat>, IChatDao
    {
        public ChatDao(DbContext ctx) :base(ctx)
        {

        }
    }
}
