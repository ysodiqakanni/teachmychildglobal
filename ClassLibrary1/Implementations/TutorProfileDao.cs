﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class TutorProfileDao : CoreDao<TutorProfile>, ITutorProfileDao
    {
        public TutorProfileDao(DbContext _dbContext) : base(_dbContext)
        {
            
        }

        public void ComputeAverageRating(long profileId)
        {
            var profile = Get(profileId);
            if (profile.Reviews.Any())
            {
                profile.AverageRating =  profile.Reviews.Select(r => r.Rating).Average();
            }
        }

        public TutorProfile GetByUserAccountId(string id)
        {
            var profile = Find(p => p.Account.Id == id).SingleOrDefault();
            return profile;
        }
        public TutorProfile GetByUniqueName(string uniqueName)
        {
            var profile = Find(p => String.Compare(p.ProfileUniqueName, uniqueName, true) == 0).FirstOrDefault();
            return profile;
        }

        public TutorProfile GetByUserAccountName(string name)
        {
            var profile = Find(p => String.Compare(p.Account.UserName, name, true) == 0).SingleOrDefault();
            return profile;
        }
    }
}
