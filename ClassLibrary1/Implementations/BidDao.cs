﻿using System.Data.Entity;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class BidDao : CoreDao<TutorialBid>, IBidDao
    {
        public BidDao(DbContext ctx) : base(ctx)
        {

        }
    }
}