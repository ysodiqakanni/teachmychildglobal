﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Data.Contracts;

namespace TeachMyChild.Data.Implementations
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private System.Data.Entity.DbContext context;// = new DbContext();

        //ICompanyRepository gcompanyRepository;   
        public UnitOfWork(System.Data.Entity.DbContext _context, IUserAccountDao _userAccountDao, ITutorProfileDao _tutorProfileDao, IEducationEntityDao _educationEntityDao,
            IWorkExperienceEntityDao _workExperienceEntityDao, ICourseDao _courseDao, ICourseCategoryDao _courseCategoryDao, ITutorialDao _tutorialDao, IParentProfileDao _parentProfileDao,
            IBidDao _bidDao, IChatDao _chatDao, IMessageDao _messageDao)
        {
            userAccountDao = _userAccountDao;
            tutorProfileDao = _tutorProfileDao;
            educationEntityDao = _educationEntityDao;
            workExperienceEntityDao = _workExperienceEntityDao;
            courseCategoryDao = _courseCategoryDao;
            courseDao = _courseDao;
            tutorialDao = _tutorialDao;
            parentProfileDao = _parentProfileDao;
            bidDao = _bidDao;
            chatDao = _chatDao;
            messageDao = _messageDao;
            context = _context;
        }
        
        public IUserAccountDao userAccountDao { get; set; }
        public ITutorProfileDao tutorProfileDao { get; set; }
        public IEducationEntityDao educationEntityDao { get; set; }
        public IWorkExperienceEntityDao workExperienceEntityDao { get; set; }
        public ICourseDao courseDao { get; set; }
        public ICourseCategoryDao courseCategoryDao { get; set; }
        public ITutorialDao tutorialDao { get; set; }
        public IBidDao bidDao { get; set; }
        public IParentProfileDao parentProfileDao { get; set; }
        public IChatDao chatDao { get; set; }
        public IMessageDao messageDao { get; set; }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
