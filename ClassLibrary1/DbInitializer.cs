﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Data
{
    public class DbInitializer : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        protected override void Seed(ApplicationDbContext context)
        {
            var categories = new List<Core.Implementations.CourseCategory>
            {
                new Core.Implementations.CourseCategory { ID = 1, Name = "Primary", DateCreated = DateTime.Now, DateModified = DateTime.Now, Description = "Primary"},
                new Core.Implementations.CourseCategory { ID = 2, Name = "Junior Secondary", DateCreated = DateTime.Now, DateModified = DateTime.Now, Description = "Junior Secondary"},
                new Core.Implementations.CourseCategory { ID = 3, Name = "Senior Secondary", Description = "Senior Secondary", DateCreated = DateTime.Now, DateModified = DateTime.Now },
                new Core.Implementations.CourseCategory { ID = 4, Name = "Foreign Languages", Description = "Foreign Languages", DateCreated = DateTime.Now, DateModified = DateTime.Now},
                new Core.Implementations.CourseCategory { ID = 5, Name = "Advanced Tech", Description = "Advanced Tech", DateCreated = DateTime.Now, DateModified = DateTime.Now },
                new Core.Implementations.CourseCategory { ID = 6, Name = "Advanced Maths", Description = "Advanced Maths", DateCreated = DateTime.Now, DateModified = DateTime.Now},
            };
            categories.ForEach(c => context.CourseCategories.Add(c));

            var courses = new List<Course>
            {
                new Course { ID = 1, Name="Mathematics", Description="Mathematics", CategoryId=1, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 2, Name="English Language", Description="English Language", CategoryId=1, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 3, Name="Basic Science and Technology", Description="Basic Science and Technology", CategoryId=1, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 4, Name="Christian Religious Knowledge", Description="Christian Religious Knowledge", CategoryId=1, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 5, Name="Islamic Studies", Description="Islamic Studies", CategoryId=1, DateCreated=DateTime.Now, DateModified=DateTime.Now },

                new Course { ID = 6, Name="Mathematics", Description="Mathematics", CategoryId=2, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 7, Name="English Language", Description="English Language", CategoryId=2, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 8, Name="Basic Science and Technology", Description="Basic Science and Technology", CategoryId=2, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 9, Name="IT", Description="IT", CategoryId=2, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 10, Name="Civic education", Description="Civic education", CategoryId=2, DateCreated=DateTime.Now, DateModified=DateTime.Now },
                new Course { ID = 11, Name="French", Description="French", CategoryId=2, DateCreated=DateTime.Now, DateModified=DateTime.Now },
            };
            courses.ForEach(c => context.Courses.Add(c));

            context.SaveChanges();

            //context.CourseCategories.Add(new Core.Implementations.CourseCategory
            //{
            //    Name = "test category",
            //    DateCreated = DateTime.Now,
            //    DateModified = DateTime.Now,
            //    Description = "test",
            //    ID = 1
            //});
            //context.SaveChanges();
            //base.Seed(context);
        }
    }
}
