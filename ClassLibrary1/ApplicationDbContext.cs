﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using System.Data.Entity;

namespace TeachMyChild.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<TutorProfile> Tutors { get; set; }

        public DbSet<Course> Courses { get; set; }
        public DbSet<CourseCategory> CourseCategories { get; set; }

        public DbSet<TutorialRequest> TutorialRequests { get; set; }
        public DbSet<ParentProfile> ParentProfiles { get; set; }
        public DbSet<Chat> Chats { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<ParentProfile>().ToTable("ParentProfiles");
        //    // base.OnModelCreating(modelBuilder);
        //}

    }

    public class DbInitializer333 : DropCreateDatabaseIfModelChanges<ApplicationDbContext>
    {
        public override void InitializeDatabase(ApplicationDbContext context)
        {
            base.InitializeDatabase(context);

        }
    }
}



// https://stackoverflow.com/a/43549456/7162741