﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Data.Contracts
{
    public interface IUserAccountDao : ICoreDao<ApplicationUser>
    {
    }
}
