﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Data.Contracts
{
    public interface IUnitOfWork
    {        
        IUserAccountDao userAccountDao { get; set; }
        ITutorProfileDao tutorProfileDao { get; set; }
        IEducationEntityDao educationEntityDao { get; set; }
        IWorkExperienceEntityDao workExperienceEntityDao { get; set; }
        ICourseDao courseDao { get; set; }
        ICourseCategoryDao courseCategoryDao { get; set; }
        ITutorialDao tutorialDao { get; set; }
        IBidDao bidDao { get; set; }
        IParentProfileDao parentProfileDao { get; set; }
        IChatDao chatDao { get; set; }
        IMessageDao messageDao { get; set; }
        void Save();
    }
}
