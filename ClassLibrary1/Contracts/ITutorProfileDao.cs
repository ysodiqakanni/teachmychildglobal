﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Data.Contracts
{
    public interface ITutorProfileDao : ICoreDao<TutorProfile>
    {
        TutorProfile GetByUserAccountId(string id);
        TutorProfile GetByUserAccountName(string name);
        TutorProfile GetByUniqueName(string uniqueName);
        void ComputeAverageRating(long profileId);
    }
}
