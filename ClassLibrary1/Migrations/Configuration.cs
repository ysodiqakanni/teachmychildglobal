namespace TeachMyChild.Data.Migrations
{
    using Core.Implementations;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<TeachMyChild.Data.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "TeachMyChild.Data.ApplicationDbContext";
        }

        protected override void Seed(TeachMyChild.Data.ApplicationDbContext context)
        {
            context.CourseCategories.AddOrUpdate(
                 p => p.ID,
                  new CourseCategory { ID = 1, Name = "Primary", DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, Description = "Primary" },
                 new CourseCategory { ID = 2, Name = "Junior Secondary", DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow, Description = "Junior Secondary" },
                 new CourseCategory { ID = 3, Name = "Senior Secondary", Description = "Senior Secondary", DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                 new CourseCategory { ID = 4, Name = "Foreign Languages", Description = "Foreign Languages", DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                 new CourseCategory { ID = 5, Name = "Advanced Tech", Description = "Advanced Tech", DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                 new CourseCategory { ID = 6, Name = "Advanced Maths", Description = "Advanced Maths", DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow }
                 );

            context.Courses.AddOrUpdate(
             p => p.ID,
                 new Course { ID = 1, Name = "Mathematics", Description = "Mathematics", CategoryId = 1, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 2, Name = "English Language", Description = "English Language", CategoryId = 1, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 3, Name = "Basic Science and Technology", Description = "Basic Science and Technology", CategoryId = 1, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 4, Name = "Christian Religious Knowledge", Description = "Christian Religious Knowledge", CategoryId = 1, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 5, Name = "Islamic Studies", Description = "Islamic Studies", CategoryId = 1, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },

                new Course { ID = 6, Name = "Mathematics", Description = "Mathematics", CategoryId = 2, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 7, Name = "English Language", Description = "English Language", CategoryId = 2, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 8, Name = "Basic Science and Technology", Description = "Basic Science and Technology", CategoryId = 2, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 9, Name = "IT", Description = "IT", CategoryId = 2, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 10, Name = "Civic education", Description = "Civic education", CategoryId = 2, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow },
                new Course { ID = 11, Name = "French", Description = "French", CategoryId = 2, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow }
             );
        }
    }
}
