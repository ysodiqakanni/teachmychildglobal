namespace TeachMyChild.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ManyToManyRelationshipBetweenTutorsAndCourses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TutorProfileCourses",
                c => new
                    {
                        TutorProfile_ID = c.Long(nullable: false),
                        Course_ID = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.TutorProfile_ID, t.Course_ID })
                .ForeignKey("dbo.TutorProfiles", t => t.TutorProfile_ID, cascadeDelete: true)
                .ForeignKey("dbo.Courses", t => t.Course_ID, cascadeDelete: true)
                .Index(t => t.TutorProfile_ID)
                .Index(t => t.Course_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TutorProfileCourses", "Course_ID", "dbo.Courses");
            DropForeignKey("dbo.TutorProfileCourses", "TutorProfile_ID", "dbo.TutorProfiles");
            DropIndex("dbo.TutorProfileCourses", new[] { "Course_ID" });
            DropIndex("dbo.TutorProfileCourses", new[] { "TutorProfile_ID" });
            DropTable("dbo.TutorProfileCourses");
        }
    }
}
