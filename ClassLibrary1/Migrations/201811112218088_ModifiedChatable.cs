namespace TeachMyChild.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedChatable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chats", "User1Id", c => c.String());
            AddColumn("dbo.Chats", "User2Id", c => c.String());
            AddColumn("dbo.Messages", "FromId", c => c.String());
            AddColumn("dbo.Messages", "ToId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Messages", "ToId");
            DropColumn("dbo.Messages", "FromId");
            DropColumn("dbo.Chats", "User2Id");
            DropColumn("dbo.Chats", "User1Id");
        }
    }
}
