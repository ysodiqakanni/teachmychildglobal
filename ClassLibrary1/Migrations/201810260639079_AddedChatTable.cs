namespace TeachMyChild.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedChatTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chats",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        TutorialRequestId = c.Long(nullable: false),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Messages",
                c => new
                    {
                        ID = c.Long(nullable: false, identity: true),
                        Content = c.String(),
                        DateCreated = c.DateTime(nullable: false),
                        DateModified = c.DateTime(nullable: false),
                        Chat_ID = c.Long(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Chats", t => t.Chat_ID)
                .Index(t => t.Chat_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Messages", "Chat_ID", "dbo.Chats");
            DropIndex("dbo.Messages", new[] { "Chat_ID" });
            DropTable("dbo.Messages");
            DropTable("dbo.Chats");
        }
    }
}
