﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface IWorkExperienceEntityService
    {
        void RemoveRange(List<WorkExperienceEntity> workExperiences);
        void AddRange(List<WorkExperienceEntity> workExperiences);
    }
}
