﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface IParentProfileService
    {
        void Save(ParentProfile parentProfile);
        ParentProfile GetProfileByUserAccountId(string id);
        ParentProfile GetProfileByUserAccountName(string username);
    }
}
