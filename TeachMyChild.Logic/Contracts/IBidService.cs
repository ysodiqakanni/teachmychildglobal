﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Logic.Contracts
{
    public interface IBidService
    {
        bool BidAlreadySubmitted(long tutorProfileId, long tutorialRequestId);
    }
}
