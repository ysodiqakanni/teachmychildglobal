﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Logic.Contracts.Utility
{
    public interface IMailer
    {
        Task SendMail(string to, string subject, string body, NetworkCredential credential);
    }
}
