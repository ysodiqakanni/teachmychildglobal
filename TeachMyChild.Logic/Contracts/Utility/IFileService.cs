﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace TeachMyChild.Logic.Contracts.Utility
{
    public interface IFileService
    {
        IDictionary<string, string> SaveFileToDisk(HttpPostedFileBase file, string serverPath, string defaultImgPath, string[] allowedExtensions);
        //bool DeleteFile(string filePath);
    }
}
