﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface IEducationEntityService
    {
        void RemoveRange(List<EducationEntity> educations);
        void AddRange(List<EducationEntity> educations);
    }
}
