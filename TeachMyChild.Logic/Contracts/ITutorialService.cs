﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface ITutorialService
    {
        void AddNewTutorialRequest(TutorialRequest tutorial);
        TutorialRequest GetById(long id);
        IEnumerable<TutorialRequest> GetAll();
        void PlaceTutorialBid(TutorialBid bid);        
    }
}
