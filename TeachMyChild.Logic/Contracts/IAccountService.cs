﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface IAccountService
    {
        bool IsAccountComplete(string username);
        string GetUserId(string username);
        ApplicationUser GetAccountByUsername(string username);
        ApplicationUser GetAccountById(string id);
        ApplicationUser GetUserByUniqueProfileName(string uniqueName);
        void Update(ApplicationUser userAccount);
    }
}
