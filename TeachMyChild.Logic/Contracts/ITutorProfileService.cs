﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface ITutorProfileService
    {
        void Save(TutorProfile tutorProfile, ApplicationUser user);
        void Update(TutorProfile tutorProfile);
        void Update(TutorProfile tutorProfile, ApplicationUser userAccount);
        TutorProfile GetProfileByUniqueName(string uniqueName);
        void ComputeAverageRating(long profileId);
        TutorProfile GetProfileByUserAccountId(string id);
        TutorProfile GetProfileByUserAccountName(string name);
        string GetProfileImageUriForTutorWithAccountId(string accountId);
        IEnumerable<TutorProfile> GetAllTutors();
        IEnumerable<TutorProfile> GetFeaturedTutors(int count);
    }
}
