﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface ICourseCategoryService
    {
        List<CourseCategory> GetAll();
        CourseCategory GetById(long id);
        void CreateNewCategory(CourseCategory category);
        void EditCategory(CourseCategory category);
        void DeleteCategory(long id);
    }
}
