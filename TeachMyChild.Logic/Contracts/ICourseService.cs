﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Logic.Contracts
{
    public interface ICourseService
    {
        List<Course> GetAll();
        Course GetById(long id);
        IEnumerable<Course> GetByIDs(IEnumerable<long> ids);
        bool CourseWithNameAlreadyExists(string name, long categoryId);
        void Save(Course course);
    }
}
