﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class TutorProfileService : ITutorProfileService
    {
        private IUnitOfWork uow;
        IAccountService accountService;
        public TutorProfileService(IUnitOfWork _uow, IAccountService _accoutService)
        {
            uow = _uow;
            accountService = _accoutService;
        }
        public void Save(TutorProfile tutorProfile, ApplicationUser user)
        {
            // string uniqueName = $"{tutorProfile.LastName}-{tutorProfile.FirstName}-{new Random().Next(9999).ToString().PadLeft(4, '0')}";   // Todo: ensure this never gives a duplicate
            tutorProfile.ProfileUniqueName = user.ProfileUniqueName;        // Todo: remove this property since it's already defined in user
            uow.tutorProfileDao.Add(tutorProfile);
            accountService.Update(user);
            uow.Save();
        }
        public void Update(TutorProfile tutorProfile)
        {
            uow.tutorProfileDao.Update(tutorProfile);           
            uow.Save();
        }
        public void ComputeAverageRating(long profileId)
        {
            uow.tutorProfileDao.ComputeAverageRating(profileId);
        }

        public TutorProfile GetProfileByUniqueName(string uniqueName)
        {
            if(String.IsNullOrEmpty(uniqueName))
                return null;
            return uow.tutorProfileDao.GetByUniqueName(uniqueName);
        }
        public TutorProfile GetProfileByUserAccountId(string id)
        {
            return uow.tutorProfileDao.GetByUserAccountId(id);
        }

        public TutorProfile GetProfileByUserAccountName(string name)
        {
            return uow.tutorProfileDao.GetByUserAccountName(name);
        }
        public string GetProfileImageUriForTutorWithAccountId(string accountId)
        {
            string result = null;
            var act = uow.userAccountDao.Get(accountId);
            if (act == null)
                throw new Exception("User account not found");
            var profile = uow.tutorProfileDao.Find(p => p.Account.Id == act.Id).FirstOrDefault();
            if(profile == null)
                throw new Exception("User profile not set up or incomplete");            
            return profile.ProfileImageUri;
        }

        public IEnumerable<TutorProfile> GetAllTutors()
        {
            return uow.tutorProfileDao.GetAll();
        }

        public void Update(TutorProfile tutorProfile, ApplicationUser userAccount)
        {
            uow.tutorProfileDao.Update(tutorProfile);
            accountService.Update(userAccount);
            uow.Save();
        }

        public IEnumerable<TutorProfile> GetFeaturedTutors(int count)
        {
            return uow.tutorProfileDao.Find(u => true).OrderByDescending(u => u.AverageRating).Take(count);
        }
    }
}
