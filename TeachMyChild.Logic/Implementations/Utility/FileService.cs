﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Logic.Contracts.Utility;
using System.Web;
using System.IO;

namespace TeachMyChild.Logic.Implementations.Utility
{
    public class FileService : IFileService
    {
        public IDictionary<string, string> SaveFileToDisk(HttpPostedFileBase file, string serverPath, string basePath, string[] allowedExtensions)
        {
            if (file == null || file.ContentLength < 1)
            {
                return new Dictionary<string, string> { { "status", "error" }, { "msg", null } };   //Json(new { status = "error", msg = Server.MapPath("~/assets/images/user.jpg") });
            }

            var imageString = file.ToString();      

            var fileName = Path.GetFileName(file.FileName); //eg myImage.jpg
            var extension = Path.GetExtension(file.FileName);    //eg .jpg

            if (allowedExtensions.Contains(extension.ToLower()))
            {
                string ordinaryFileName = Path.GetFileNameWithoutExtension(file.FileName);
                string myFile = ordinaryFileName + "_" + Guid.NewGuid() + extension;
                var path = Path.Combine(serverPath, myFile);
                file.SaveAs(path);

                string relativePath = basePath + "/" + myFile;
                return new Dictionary<string, string> { { "status", "success" }, { "msg", relativePath } };
            }
            else
            {
                return new Dictionary<string, string> { { "status", "error" }, { "msg", "File format not supported!" } };
            }
        }       
    }
}
