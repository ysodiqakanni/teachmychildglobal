﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Logic.Contracts.Utility;

namespace TeachMyChild.Logic.Implementations.Utility
{
    public class Mailer : IMailer
    {
        public async Task SendMail(string to, string subject, string body, NetworkCredential credential)
        {
            var message = new MailMessage();
            if (!isValidEmail(to))
                throw new FormatException("The to-email does not have a valid format");
            
            message.To.Add(new MailAddress(to));
            message.From = new MailAddress("ysodiqakanni@gmail.com", "Teach My Child Global");
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            try
            {
                using (var smtp = new SmtpClient())
                {
                    smtp.Credentials = credential;
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;

                    //smtp.SendCompleted += new SendCompletedEventHandler(Smtp_SendCompleted);

                    await Task.Run(() => smtp.Send(message));
                }
            }
            catch (Exception ex)
            {
                // Todo:: log mail sending failure and continue
                // log the exception first
                 throw;
            }
        }

        private bool isValidEmail(string email)
        {
            // Todo:: complete logic
            // use regex to validate email
            // throw new NotImplementedException();
            return true;
        }
    }
}
