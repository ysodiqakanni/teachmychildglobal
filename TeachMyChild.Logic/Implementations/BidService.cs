﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class BidService : IBidService
    {
        IUnitOfWork uow;
        public BidService(IUnitOfWork _uow)
        {
            uow = _uow;
        }
        public bool BidAlreadySubmitted(long tutorProfileId, long tutorialRequestId)
        {
            return uow.bidDao.GetAll().Any(b => b.Bidder.ID == tutorProfileId && b.Tutorial.ID == tutorialRequestId);
        }
    }
}
