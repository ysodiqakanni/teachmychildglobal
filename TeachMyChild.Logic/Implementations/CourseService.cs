﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class CourseService : ICourseService
    {
        IUnitOfWork uow;
        public CourseService(IUnitOfWork _uow)
        {
            uow = _uow;

        }

        public bool CourseWithNameAlreadyExists(string name, long categoryId)
        {
            return uow.courseDao.GetAll().Any(c => c.CategoryId == categoryId && String.Compare(c.Name, name, true) == 0);
        }

        public List<Course> GetAll()
        {
            return uow.courseDao.GetAll().ToList();
        }
        public Course GetById(long id)
        {
            var course = uow.courseDao.Get(id);
            return course;
        }
        public IEnumerable<Course> GetByIDs(IEnumerable<long> ids)
        {
            // since there is a maximum of 5 ids to come in here, it should be optimal to call get 5 times instead of Find
            // Todo: Compare the performances of Get and FInd
            var result = new List<Course>();
            foreach (var id in ids)
            {
                var course = GetById(id);
                if (course != null)
                    result.Add(course);
            }
            return result;
        }

        public void Save(Course course)
        {
            if (course == null)
                throw new Exception("Course cannot be null");            
            uow.courseDao.Add(course);
            uow.Save();
        }
    }
}
