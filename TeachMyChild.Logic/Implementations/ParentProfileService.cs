﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class ParentProfileService : IParentProfileService
    {
        private IUnitOfWork uow;
        public ParentProfileService(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public ParentProfile GetProfileByUserAccountId(string id)
        {
            if (String.IsNullOrEmpty(id))
                return null;
            return uow.parentProfileDao.GetByUserAccountId(id);
        }

        public ParentProfile GetProfileByUserAccountName(string username)
        {
            if (String.IsNullOrEmpty(username))
                return null;
            return uow.parentProfileDao.GetByUserAccountName(username);
        }

        public void Save(ParentProfile parentProfile)
        {
            uow.parentProfileDao.Add(parentProfile);
            uow.Save();
        }
    }
}
