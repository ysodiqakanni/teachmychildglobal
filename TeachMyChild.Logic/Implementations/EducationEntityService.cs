﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class EducationEntityService : IEducationEntityService
    {
        private IUnitOfWork uow;
        public EducationEntityService(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public void AddRange(List<EducationEntity> educations)
        {
            uow.educationEntityDao.AddRange(educations);
        }

        public void RemoveRange(List<EducationEntity> educations)
        {
            uow.educationEntityDao.RemoveRange(educations);
        }
    }
}
