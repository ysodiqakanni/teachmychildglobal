﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class TutorialService : ITutorialService
    {
        IUnitOfWork uow;
        public TutorialService(IUnitOfWork _uow)
        {
            uow = _uow;
        }
        public void AddNewTutorialRequest(TutorialRequest tutorial)
        {
            uow.tutorialDao.Add(tutorial);
            uow.Save();
        }  

        public IEnumerable<TutorialRequest> GetAll()
        {
            return uow.tutorialDao.GetAll();
        }

        public TutorialRequest GetById(long id)
        {
            var tutorial = uow.tutorialDao.Get(id);
            return tutorial;
        }

        public void PlaceTutorialBid(TutorialBid bid)
        {
            uow.bidDao.Add(bid);
            uow.Save();
        }
    }
}
