﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class WorkExperienceEntityService : IWorkExperienceEntityService
    {
        private IUnitOfWork uow;
        public WorkExperienceEntityService(IUnitOfWork _uow)
        {
            uow = _uow;
        }

        public void AddRange(List<WorkExperienceEntity> workExperiences)
        {
            uow.workExperienceEntityDao.AddRange(workExperiences);
        }

        public void RemoveRange(List<WorkExperienceEntity> workExperiences)
        {
            uow.workExperienceEntityDao.RemoveRange(workExperiences);
        }
    }
}
