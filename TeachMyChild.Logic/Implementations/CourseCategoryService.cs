﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class CourseCategoryService : ICourseCategoryService
    {
        IUnitOfWork uow;
        public CourseCategoryService(IUnitOfWork _uow)
        {
             uow = _uow;
        }

        public void CreateNewCategory(CourseCategory category)
        {
            if (category == null)
                throw new Exception("Category cannot be null");
            uow.courseCategoryDao.Add(category);
            uow.Save();
        }

        public void DeleteCategory(long id)
        {
            throw new NotImplementedException();
        }

        public void EditCategory(CourseCategory category)
        {
            if (category == null)
                throw new Exception("Category cannot be null");
            uow.courseCategoryDao.Update(category);
            uow.Save();
        }

        public List<CourseCategory> GetAll()
        {
            return uow.courseCategoryDao.GetAll().ToList();
        }

        public CourseCategory GetById(long id)
        {
            return uow.courseCategoryDao.Get(id);
        }
    }
}
