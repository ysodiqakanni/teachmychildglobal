﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    public class AccountService : IAccountService
    {
        private IUnitOfWork uow;
        public AccountService(IUnitOfWork _uow)
        {
            uow = _uow;
        }
        public bool IsAccountComplete(string username)
        {
            if (String.IsNullOrEmpty(username))
                throw new ArgumentNullException();
            var userAccount = uow.userAccountDao.Find(u => String.Compare(username, u.Email, true) == 0).FirstOrDefault();
            if (userAccount == null)
                throw new Exception("User account does not exist");
            return userAccount.IsAccountComplete;
        }
        public string GetUserId(string username)
        {
            if (String.IsNullOrEmpty(username))
                throw new ArgumentNullException();
            var userAccount = uow.userAccountDao.Find(u => String.Compare(username, u.UserName, true) == 0).FirstOrDefault();
            if (userAccount == null)
                throw new Exception("User account does not exist");
            return userAccount.Id;
        }

        public ApplicationUser GetAccountByUsername(string username)
        {
            if (String.IsNullOrEmpty(username))
                throw new ArgumentNullException();
            var userAccount = uow.userAccountDao.Find(u => String.Compare(username, u.UserName, true) == 0).FirstOrDefault();
            return userAccount;
        }

        public ApplicationUser GetAccountById(string id)
        {
            if (String.IsNullOrEmpty(id))
                throw new ArgumentNullException();
            var userAccount = uow.userAccountDao.Find(u => String.Compare(id, u.Id, true) == 0).FirstOrDefault();
            return userAccount;
        }
        public void Update(ApplicationUser user)
        {
            uow.userAccountDao.Update(user);
        }

        public ApplicationUser GetUserByUniqueProfileName(string uniqueName)
        {
            if (String.IsNullOrEmpty(uniqueName))
                return null;
            var theUser = uow.userAccountDao.Find(u => String.Compare(uniqueName, u.ProfileUniqueName, true) == 0).FirstOrDefault();
            return theUser;
        }
    }
}
