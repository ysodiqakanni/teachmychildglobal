﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Logic.Implementations
{
    
    public class ChatService : IChatService
    {
        IUnitOfWork uow;
        public ChatService(IUnitOfWork _uow)
        {
            uow = _uow;
        }
        public IEnumerable<Chat> GetAll()
        {
            return uow.chatDao.GetAll();            
        }

        public IEnumerable<Chat> GetAllChatsForUser(string userId)
        {
            return uow.chatDao.Find(c => c.User1Id == userId || c.User2Id == userId);            
        }
    }
}
