﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using TeachMyChild.Core.Enums;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TeachMyChild.Core.Implementations
{
    public class ApplicationUser : IdentityUser
    {
        [MaxLength(30, ErrorMessage = "Name must not exceed 30 characters")]        
        public string LastName { get; set; }

        [MaxLength(30, ErrorMessage = "Name must not exceed 30 characters")]
        public string FirstName { get; set; }
        public AccountType AccountType { get; set; }
        public bool IsAccountComplete { get; set; }

        [Index(IsUnique = true), MaxLength(100)]
        // not specifying the maxlength wil cause sql to use nvarchar(max) for the columns and that'll throw an error.
        // see https://stackoverflow.com/questions/30298629/is-of-a-type-that-is-invalid-for-use-as-a-key-column-in-an-index/30298748
        public string ProfileUniqueName { get; set; }

        public DateTime DateCreated { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }
}
