﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class WorkExperienceEntity : Entity, IWorkExperienceEntity
    {
        public string CompanyName { get; set; }
        public string JobTitle { get; set; }
        public string Description { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool StillWorksHere { get; set; } 
    }
}
