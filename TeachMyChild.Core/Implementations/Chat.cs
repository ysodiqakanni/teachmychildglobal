﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class Chat : Entity, IChat
    {
        public long TutorialRequestId { get; set; }
        public string User1Id { get; set; }     // User who initiated the chat
        public string User2Id { get; set; }
        // Chat status : Open, closed, reopened, blockedUser

        public virtual List<Message> Messages { get; set; }
    }
}
