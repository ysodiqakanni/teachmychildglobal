﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class Review : Entity, IReview
    {
        public double Rating { get; set;}

        public string ReviewerName { get; set; }
   

        public string ReviewerImageUri { get; set; }
     

        public string TextContent { get; set; }
   
    }
}
