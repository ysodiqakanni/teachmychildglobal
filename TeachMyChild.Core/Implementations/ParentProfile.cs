﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class ParentProfile : Entity, IParentProfile
    {
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public virtual ApplicationUser Account { get; set; }
        public virtual List<TutorialRequest> PostedTutorials { get; set; }
    }
}
