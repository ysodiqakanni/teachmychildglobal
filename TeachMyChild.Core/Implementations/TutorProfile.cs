﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class TutorProfile : Entity, ITutorProfile
    {
        [Index(IsUnique = true), MaxLength(100)]
        // not specifying the maxlength wil cause sql to use nvarchar(max) for the columns and that'll throw an error.
        // see https://stackoverflow.com/questions/30298629/is-of-a-type-that-is-invalid-for-use-as-a-key-column-in-an-index/30298748
        public string ProfileUniqueName { get; set; }   // must be unique
        [MaxLength(30, ErrorMessage ="Name must not exceed 30 characters")]
        public string LastName { get; set; }

        [MaxLength(30, ErrorMessage = "Name must not exceed 30 characters")]
        public string FirstName { get; set; }

        [MaxLength(30, ErrorMessage = "Name must not exceed 30 characters")]
        public string MiddleName { get; set; }
        public string Gender { get; set; }
        public string Phone { get; set; }

        public string About { get; set; }
        public string HomeAddress { get; set; }
        public string City { get; set; }        
        public string State { get; set; }        
        public string ZipCode { get; set; }
        public string LinkedInUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string FacebookUrl { get; set; }
        public int LastStepCompleted { get; set; }

        // DOCUMENTS
        public string ResumeUri { get; set; }
        public string IdentificationUri { get; set; }
        public string UtilityBilUri { get; set; }
        public string ProfileImageUri { get; set; }
        public string VideoCvUri { get; set; }
        public string OtherFilesUris { get; set; } // URIs separated by comma



        public string Email { get { return Account?.Email; } }
        public Decimal HourlyRate { get; set; }
        //public int Rating { get; set; }
        public int HoursCompleted { get; set; }
        public DateTime DateOfBirth { get; set; } = new DateTime(1990, 1, 1); // just any default date to avoid error in the db
        public double AverageRating { get; set; }
        public int NumberOfCompletedTutorials { get; set; }
        public int NumberOfOngoingTutorials { get; set; }
        public decimal TotalEarnings { get; set; }


        public virtual ApplicationUser Account { get; set; }
        public virtual List<Review> Reviews { get; set; }
        public virtual List<Course> Courses { get; set; }
        public virtual List<WorkExperienceEntity> WorkExperiences { get; set; }
        public virtual List<EducationEntity> Educations { get; set; }

        public virtual List<TutorialRequest> Tutorials { get; set; }
    }
}
