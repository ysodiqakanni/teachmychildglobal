﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class EducationEntity : Entity, IEducationEntity
    {
        public string InstitutionName { get; set; }
        
        public string Concentration { get; set; }

        public string DegreeAwarded { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool StillStudiesHere { get; set; } = false;
    }
}
