﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;
using TeachMyChild.Core.Enums;

namespace TeachMyChild.Core.Implementations
{
    public class TutorialRequest : Entity, ITutorialRequest
    {
        public string ShortSummary { get; set; }
        public string Details { get; set; }
        public int NumberOfHoursInADay { get; set; }
        public int NumberOfDaysInAWeeek { get; set; }
        public string TimeOfDay { get; set; }
        public decimal ProposedChargePerHour { get; set; }

        public string StartTime { get; set; } // eg 9am
        public string EndTime { get; set; } // eg 11am

        public int NumberOfStudents { get; set; }
        public string City { get; set; }
        public string State { get; set; }

        public decimal AcceptedChargePerHour { get; set; }
        public bool OrderAccepted { get; set; }
        public bool IsTutorialAwarded { get; set; }
        public PaymentFrequency PaymentFrequency { get; set; }
        public TutorialStatus Status { get; set; }


        public virtual Course Course { get; set; }
        public virtual ParentProfile Poster { get; set; }
        public virtual TutorProfile AssignedTutor { get; set; }
        // public virtual List<TutorProfile> Bidders { get; set; }
        public virtual List<TutorialBid> Bids { get; set; }  // we can get the bidders from this  



        public virtual List<TutorialClass> Classes { get; set; }
    }
}
