﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class Message : Entity, IMessage
    {
        // Date sent = DateCreated
        public string FromId { get; set; }
        public string ToId { get; set; }
        public string Content { get; set; }
        // To 
        // Fro
    }
}
