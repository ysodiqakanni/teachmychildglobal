﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class TutorialClass : Entity, ITutorialClass
    {
        public DateTime? CheckInTime { get; set; }
        public DateTime? CheckOutTime { get; set; }

        public int NumberOfHours { get; set; }      // Difference between CheckOut and CheckIn times or manually entered
        public bool NumberOfHoursAgreedByLearnerAndParent { get; set; }     // The tutor must agree with the number of hours entered by the parent to ensure no issues regarding incomplete hours upon payout
        public decimal AmoutDue { get; set; }   // This should be the numberOfHours * ChargePerHour
        public decimal TippedAmmount { get; set; }      // Tips can be added upon satisfaction
        // Ratings and Remarks to be used to create Tutor's reviews
        public int Rating { get; set; }    // To be set by parent and sent to Tutor for awareness and improvement
        public string Remarks { get; set; }
    }
}
