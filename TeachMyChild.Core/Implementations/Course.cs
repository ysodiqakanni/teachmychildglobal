﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class Course : Entity, ICourse
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string ImageUri { get; set; }

        [ForeignKey("Category")]
        public long CategoryId { get; set; }

        public virtual CourseCategory Category { get; set; }
        public virtual List<TutorProfile> Tutors { get; set; }
    }
}
