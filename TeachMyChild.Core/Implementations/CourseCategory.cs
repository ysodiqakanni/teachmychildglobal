﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class CourseCategory : Entity, ICourseCategory
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual List<Course> Courses { get; set; }
    }
}
