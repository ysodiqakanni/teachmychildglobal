﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Contracts;

namespace TeachMyChild.Core.Implementations
{
    public class TutorialBid : Entity, ITutorialBid
    {
        public decimal Amount { get; set; }
        public string Proposal { get; set; }
        public virtual TutorialRequest Tutorial { get; set; }
        public virtual TutorProfile Bidder { get; set; }
    }
}
