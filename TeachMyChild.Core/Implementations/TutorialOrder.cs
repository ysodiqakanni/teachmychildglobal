﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Core.Implementations
{
    public class TutorialOrder
    {
        public decimal AcceptedChargePerHour { get; set; }
        public bool OrderAccepted { get; set; }
    }
    public enum PaymentFrequency
    {
        AfterEachClass, Weekly, Monthly, OnRequest
    }
}
