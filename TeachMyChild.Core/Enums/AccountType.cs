﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Core.Enums
{
    public enum AccountType
    {
        None, Tutor, Parent, Admin
    }
}
