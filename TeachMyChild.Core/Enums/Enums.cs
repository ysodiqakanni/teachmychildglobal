﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Core.Enums
{
    public enum PaymentFrequency
    {
        AfterEachClass, Weekly, Monthly, OnRequest
    }
    public enum TutorialStatus
    {
        Applied, Started, Completed, Canceled
    }
    public enum MessageStatus
    {
        Sent, Delivered, Read
    }
    public enum ChatStatus
    {
        Open, Close, Reopened, Blocked
    }
}
