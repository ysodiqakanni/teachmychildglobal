﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Core.Contracts
{
    interface IWorkExperienceEntity : IEntity
    {
        string CompanyName { get; set; }
        string JobTitle { get; set; }
        string Description { get; set; }
        DateTime StartDate { get; set; }
        DateTime EndDate { get; set; }
        bool StillWorksHere { get; set; }
    }
}
