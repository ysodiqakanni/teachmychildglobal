﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Core.Contracts
{
    public interface ICourse : IEntity
    {
        string Name { get; set; }
        string Description { get; set; }
        string ImageUri { get; set; }

        CourseCategory Category { get; set; }
    }
}
