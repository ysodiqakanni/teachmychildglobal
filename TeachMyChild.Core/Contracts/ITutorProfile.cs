﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Core.Contracts
{
    interface ITutorProfile : IEntity
    {
        string ProfileUniqueName { get; set; }
        string LastName { get; set; }
        string FirstName { get; set; }
        string MiddleName { get; set; }
        string Gender { get; set; }
        string Phone { get; set; }
        string Email { get; }
        string About { get; set; }
        string HomeAddress { get; set; }
        string City { get; set; }
        string State { get; set; }
        string ZipCode { get; set; }
        string LinkedInUrl { get; set; }
        string TwitterUrl { get; set; }
        string FacebookUrl { get; set; }
        Decimal HourlyRate { get; set; }
        //int Rating { get; set; }
        int HoursCompleted { get; set; }
        DateTime DateOfBirth { get; set; }
        double AverageRating { get; set; }
        int LastStepCompleted { get; set; }
        int NumberOfCompletedTutorials { get; set; }
        int NumberOfOngoingTutorials { get; set; }
        decimal TotalEarnings { get; set; }

        // DOCUMENTS
        string ResumeUri { get; set; }
        string IdentificationUri { get; set; }
        string UtilityBilUri { get; set; }
        string ProfileImageUri { get; set; }
        string VideoCvUri { get; set; }
        string OtherFilesUris { get; set; } // URIs separated by comma
        // DOCUMENTS

        ApplicationUser Account { get; set; }
        List<Review> Reviews { get; set; }
        List<WorkExperienceEntity> WorkExperiences { get; set; }
        List<EducationEntity> Educations { get; set; }
    }
}
