﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Core.Contracts
{
    public interface IParentProfile : IEntity
    {
        string LastName { get; set; }
        string FirstName { get; set; }
        ApplicationUser Account { get; set; }
    }
}

