﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Core.Contracts
{
    public interface ITutorialRequest : IEntity
    {
        string Details { get; set; }
        int NumberOfHoursInADay { get; set; }
        int NumberOfDaysInAWeeek { get; set; }
        string TimeOfDay { get; set; }
        decimal ProposedChargePerHour { get; set; }
        //decimal AcceptedChargePerHour { get; set; }
        int NumberOfStudents { get; set; }
        string City { get; set; }
        string State { get; set; }
        bool IsTutorialAwarded { get; set; }


        ParentProfile Poster { get; set; }
        TutorProfile AssignedTutor { get; set; }
        Course Course { get; set; }
    }
}
