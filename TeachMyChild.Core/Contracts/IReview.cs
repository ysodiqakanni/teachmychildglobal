﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Core.Contracts
{
    public interface IReview : IEntity
    {
        string ReviewerName { get; set; }
        string TextContent { get; set; }
        string ReviewerImageUri { get; set; }
        double Rating { get; set; }
    }
}
