﻿using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Core.Contracts
{
    public interface ITutorialBid : IEntity
    {
        decimal Amount { get; set; }
        string Proposal { get; set; }
        TutorialRequest Tutorial { get; set; }
        TutorProfile Bidder { get; set; }
    }
}