﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeachMyChild.Core.Contracts
{
    interface IEducationEntity : IEntity
    {
         string InstitutionName { get; set; }
         string Concentration { get; set; }        
         string DegreeAwarded { get; set; }        
         DateTime StartDate { get; set; }        
         DateTime EndDate { get; set; }
         bool StillStudiesHere { get; set; } 
    }
}
