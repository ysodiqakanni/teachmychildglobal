﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.Core.Contracts
{
    public interface ICourseCategory : IEntity
    {
        string Name { get; set; }
        string Description { get; set; }
        List<Course> Courses { get; set; }
    }
}
