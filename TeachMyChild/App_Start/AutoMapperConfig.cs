﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TeachMyChild.Core.Implementations;
using TeachMyChild.ViewModels;

namespace TeachMyChild.App_Start
{
    public class AutoMapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<ProfileBasics, TutorProfile>()
                .ForMember(tp => tp.HomeAddress, opt => opt.MapFrom(src => src.HomeAddressLine1 + "\n" + src.HomeAddressLine2 + "\n" + src.HomeAddressLine3));
                cfg.CreateMap<TutorProfile, ProfileBasics>()
                .ForMember(pb => pb.HomeAddressLine1, opt => opt.MapFrom(tp => tp.HomeAddress.Split('\n')[0]))
                .ForMember(pb => pb.HomeAddressLine2, opt => opt.MapFrom(tp => tp.HomeAddress.Split('\n')[1]))
                .ForMember(pb => pb.HomeAddressLine3, opt => opt.MapFrom(tp => tp.HomeAddress.Split('\n')[2]));

                cfg.CreateMap<TutorProfile, EduAndProfessionalBackground>();
            });
        }
    }
}