﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SimpleInjector;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Data.Implementations;
using TeachMyChild.Logic.Contracts;
using TeachMyChild.Logic.Contracts.Utility;
using TeachMyChild.Logic.Implementations;
using TeachMyChild.Logic.Implementations.Utility;

namespace TeachMyChild
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //Database.SetInitializer<ApplicationDbContext>(new DbInitializer());

            var container = new Container();

            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
            container.Register<System.Data.Entity.DbContext>(() => new ApplicationDbContext(), Lifestyle.Scoped);

            container.Register<IUserStore<ApplicationUser>, MyUserStore>(Lifestyle.Scoped);
            container.Register<UserManager<ApplicationUser>, ApplicationUserManager>(Lifestyle.Scoped);

            container.Register<IAuthenticationManagerFactory, AuthenticationManagerFactory>();
            container.Register<IAuthenticationManager>(
                    () => HttpContext.Current.GetOwinContext().Authentication);



            // Registering Repository Interfaces           
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<IUserAccountDao, UserAccountDao>(Lifestyle.Scoped);
            container.Register<ITutorProfileDao, TutorProfileDao>(Lifestyle.Scoped);
            container.Register<IEducationEntityDao, EducationEntityDao>(Lifestyle.Scoped);
            container.Register<IWorkExperienceEntityDao, WorkExperienceEntityDao>(Lifestyle.Scoped);
            container.Register<ICourseDao, CourseDao>(Lifestyle.Scoped);
            container.Register<ICourseCategoryDao, CourseCategoryDao>(Lifestyle.Scoped);
            container.Register<ITutorialDao, TutorialDao>(Lifestyle.Scoped);
            container.Register<IBidDao, BidDao>(Lifestyle.Scoped);
            container.Register<IChatDao, ChatDao>(Lifestyle.Scoped);
            container.Register<IMessageDao, MessageDao>(Lifestyle.Scoped);
            container.Register<IParentProfileDao, ParentProfileDao>(Lifestyle.Scoped);


            // Registering Service Interfaces
            container.Register<IAccountService, AccountService>(Lifestyle.Scoped);
            container.Register<ITutorProfileService, TutorProfileService>(Lifestyle.Scoped);
            container.Register<IEducationEntityService, EducationEntityService>(Lifestyle.Scoped);
            container.Register<IWorkExperienceEntityService, WorkExperienceEntityService>(Lifestyle.Scoped);
            container.Register<ICourseCategoryService, CourseCategoryService>(Lifestyle.Scoped);
            container.Register<ICourseService, CourseService>(Lifestyle.Scoped);
            container.Register<ITutorialService, TutorialService>(Lifestyle.Scoped);
            container.Register<IParentProfileService, ParentProfileService>(Lifestyle.Scoped);
            container.Register<IBidService, BidService>(Lifestyle.Scoped);
            container.Register<IChatService, ChatService>(Lifestyle.Scoped);
            container.Register<IMessageService, MessageService>(Lifestyle.Scoped);
            container.Register<IFileService, FileService>(Lifestyle.Scoped);

            //container.Verify();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
            // Initialize automapped classes
            App_Start.AutoMapperConfig.Initialize();
        }
    }
}
