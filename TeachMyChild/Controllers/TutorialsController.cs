﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Helpers;
using TeachMyChild.Logic.Contracts;
using TeachMyChild.Utilities;
using TeachMyChild.ViewModels;

namespace TeachMyChild.Controllers
{
    public class TutorialsController : BaseController
    {
        ApplicationUserManager userManager;
        ITutorialService tutorialService;
        ICourseService courseService;
        ITutorProfileService tutorProfileService;
        IParentProfileService parentProfileService;
        IBidService bidService;
        public TutorialsController(ApplicationUserManager _userManager, ITutorialService tutorialService, ICourseService _courseService, ITutorProfileService tutorProfileService, IParentProfileService parentProfileService, IBidService _bidService)
        {
            userManager = _userManager;
            this.tutorialService = tutorialService;
            this.tutorProfileService = tutorProfileService;
            this.parentProfileService = parentProfileService;
            bidService = _bidService;
            courseService = _courseService;
        }
        [Authorize]
        // GET: Tutorials
        public async Task<ActionResult> Index()
        {
            var tutorials = tutorialService.GetAll().OrderByDescending(t => t.DateCreated);
            //if(tutorials.Count() == 0)
            //{
            //    var testTutorials = new List<TutorialRequest>()
            //    {
            //        new TutorialRequest
            //        {
            //            ShortSummary = "I need a maths tutor for my children",
            //            Details = @"Lorem, ipsum dolor sit amet consectetur adipisicing elit. Distinctio repellat,
            //                                veniam totam asperiores aut
            //                                aspernatur repudiandae, eos quia numquam cupiditate reiciendis dolore facere,
            //                                soluta est maiores harum
            //                                ullam earum culpa",
            //            City="Yaba",
            //            State = "Lagos", ProposedChargePerHour =1650, DateCreated = DateTime.Now, Course =new Course { Name="Chemistry"},//  Courses = new List<Course> {new Course { Name="Chemistry"}, new Course { Name= "Physics"} },
            //            Poster = GetLoggedInParentProfile()
            //        }
            //    };
            //    return View(testTutorials);
            //}
            return View(tutorials);
        }
        [Authorize(Roles = "Parent")]
        public ActionResult NewRequest()
        {
            var nigeriaStates = P.GetStates("nigeria");
            ViewBag.States = nigeriaStates.AsEnumerable().Select(i => new SelectListItem { Text = i, Value = i });
            var allCourses = courseService.GetAll();
            ViewBag.Courses = allCourses.AsEnumerable().Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() });

            //CreateNewTutorialRequestVM model = new CreateNewTutorialRequestVM
            //{
            //    CourseListItems = allCourses.Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() })
            //};
            return View();
        }

        [Authorize(Roles ="Parent")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> NewRequest(CreateNewTutorialRequestVM model, string CourseId)
        {
            var nigeriaStates = P.GetStates("nigeria");
            ViewBag.States = nigeriaStates.AsEnumerable().Select(i => new SelectListItem { Text = i, Value = i });
            var allCourses = courseService.GetAll();
            ViewBag.Courses = allCourses.AsEnumerable().Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() });
            if (String.IsNullOrEmpty(CourseId))
            {
                ModelState.AddModelError("", "Please select a course");
                return View(model);
            }
            //var loggedInUsername = User.Identity.Name;
            var user = await GetLoggedInUserAccount();//  userManager.FindByNameAsync(loggedInUsername);
            
            
            if (ModelState.IsValid)
            {
                try
                {
                    var chosenCourse = allCourses.Find(c => c.ID == int.Parse(CourseId));//  courseService.GetById(int.Parse(CourseId));
                    if (chosenCourse == null)
                    {
                        ModelState.AddModelError("", "Please select a course");
                        return View(model);
                    }
                    TutorialRequest tutorial = new TutorialRequest()
                    {
                        City = model.City,
                        ShortSummary = model.ShortSummary,
                        Details = model.Description,
                        NumberOfHoursInADay = model.NumberOfHoursInADay,
                        NumberOfDaysInAWeeek = model.NumberOfDaysInAWeeek,
                        NumberOfStudents = model.NumberOfStudents,
                        ProposedChargePerHour = model.ProposedChargePerHour,
                        Course = chosenCourse,
                        State = model.State,
                        Poster = GetLoggedInParentProfile(), 
                        
                    };
                    tutorialService.AddNewTutorialRequest(tutorial);
                    ViewBag.Msg = "You request has been posted";
                    return View();
                }
                catch (Exception ex)
                {
                    ViewBag.Msg = "An error occured while processing your request and we are currently investigating it.";
                    throw;
                }
       
               
            }
            return View();
        }

        [Authorize(Roles ="Tutor")]
        public ActionResult Bid()
        {
            return View();
        }

        [Authorize(Roles = "Tutor")]
        [HttpPost]
        public async Task<ActionResult> Bid(PlaceBidVM model)
        {
            // Todo: Validate the proposal amount (max and min vals) properly
            decimal bidAmount = 0;
            if(!decimal.TryParse(model.Amount, out bidAmount) || bidAmount < AppConstants.MINIMUM_HOURLY_RATE || bidAmount > AppConstants.MAXIMUM_HOURLY_RATE)
            {
                return Json(new { success = false, msg = "Invalid amount" });                
            }
            var theTutorialRequest = tutorialService.GetById(model.TutorialRequestId);
            if(theTutorialRequest == null)
                return Json(new { success = false, msg = "This tutorial request does not exist again" });            
            var theTutor = await GetLoggedInTutorProfile();
            if (bidService.BidAlreadySubmitted(theTutor.ID, theTutorialRequest.ID))
                return Json(new { success = false, msg = "You have already submitted a bid for this tutorial" });

            try
            {
                TutorialBid bid = new TutorialBid
                {
                    Amount = bidAmount,
                    Proposal = model.Proposal,
                    Bidder = theTutor,
                    Tutorial = theTutorialRequest
                };
                tutorialService.PlaceTutorialBid(bid);
                return Json(new { success = true, msg = "Bid successfully placed" });
            }
            catch (Exception ex)
            {
                // Todo: log ex
                return Json(new { success = false, msg = "An error occured while placing your bid" });
            }            
        }
        [Authorize(Roles = AppConstants.TUTOR_ROLE_NAME + "," + AppConstants.PARENT_ROLE_NAME)]
        public async Task<ActionResult> Details(long? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var tutorialRequest = tutorialService.GetById(id.Value);
            if (tutorialRequest == null)
                return HttpNotFound();
            var user = await GetLoggedInUserAccount();
            if (await userManager.IsInRoleAsync(user.Id, AppConstants.TUTOR_ROLE_NAME))
                ViewBag.LoggedInUser = AppConstants.TUTOR_ROLE_NAME;
            return View(tutorialRequest);
        }

        private async Task<TutorProfile> GetLoggedInTutorProfile()
        {
            var username = User.Identity.Name;
            if (String.IsNullOrEmpty(username))
                return null;
            return tutorProfileService.GetProfileByUserAccountName(username);
        }
        private ParentProfile GetLoggedInParentProfile()
        {
            var username = User.Identity.Name;
            if (String.IsNullOrEmpty(username))
                return null;
            return parentProfileService.GetProfileByUserAccountName(username);
        }
        private async Task<ApplicationUser> GetLoggedInUserAccount()
        {
            return await userManager.FindByNameAsync(User.Identity.Name);
        }
    }
}