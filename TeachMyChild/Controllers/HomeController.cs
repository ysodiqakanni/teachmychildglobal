﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TeachMyChild.Helpers;
using TeachMyChild.Logic.Contracts;
using TeachMyChild.Logic.Implementations.Utility;
using TeachMyChild.ViewModels;

namespace TeachMyChild.Controllers
{
    public class HomeController : Controller
    {
        ITutorProfileService tutorService;
        public HomeController(ITutorProfileService tutorSvc)
        {
            tutorService = tutorSvc;
        }
        public ActionResult Index()
        {
            IndexViewModel model = new ViewModels.IndexViewModel();
            // get top 4 tutors
            // get for next, for the second page of carousel
            ViewBag.DefaultTutorProfileImgUri = AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI;
            var featuredTutors = tutorService.GetFeaturedTutors(4).ToList();
            model.featuredTutors = featuredTutors.Select(f => new TutorModel { ProfileImageUri = f.ProfileImageUri, UniqueName = f.ProfileUniqueName, FullName = f.FirstName + " "+f.LastName, Subjects = String.Join(", ", f.Courses.Select(c=>c.Name)) }).ToList();
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "A little bit about us";

            return View();
        }

        public ActionResult Contact()
        {
            
            ViewBag.Message = "We wanna hear from you.";
           
            return View();
        }
        [HttpPost]
        public async Task<ActionResult> Contact(ContactViewModel model)
        {
            ViewBag.Msg = "We wanna hear from you.";
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                string to = AppConstants.DESTINATION_EMAIL_FOR_GENERAL_CONTACT;

                string subject = model.Subject;
                string senderName = model.Name?? "";
                string senderPhone = model.Phone?? "";
                var credentials = AppConstants.MAIL_CREDENTIALS;
                string body = $"New mail by {senderName}, Phone = {senderPhone} \nBody->> {model.Body}";

                Mailer mailer = new Mailer();
                // Todo: Mails should be saved to db first and sent later to avoid failures
                await mailer.SendMail(to, subject, body, credentials);
                ViewBag.Msg = "Thanks for contacting us";
            }
            catch (Exception ex)
            {
                ViewBag.Msg = "An error occured while sending your message";
                return View(model);
            }
            return View();
        }
    }
}