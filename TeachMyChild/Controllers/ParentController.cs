﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachMyChild.Helpers;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Controllers
{
    [Authorize(Roles ="Parent")]
    public class ParentController : BaseController
    {
        IAccountService actService;
        public ParentController(IAccountService _actService)
        {
            actService = _actService;
        }
        // GET: Parent
        public ActionResult Index()
        {
            return View();
        }

        // GET: Parent/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Parent/Create
        public ActionResult Create(string id)
        {
            var user = actService.GetAccountById(id);
            if(user == null)
            {
                TempData[KeyRings.ERROR_MESSAGE] = "Your account does not exist";
                return RedirectToAction("Error");
            }
            if (user.IsAccountComplete)
            {
                TempData[KeyRings.ERROR_MESSAGE] = "Your profile is complete, please login to your account";
                return RedirectToAction("Error");
            }
            return View();
        }

        // POST: Parent/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Dashboard()
        {
            return View();
        }

        // GET: Parent/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Parent/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Message()
        {
            return View();
        }
    }
}
