﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Helpers;
using TeachMyChild.Logic.Contracts;
using TeachMyChild.Logic.Contracts.Utility;
using TeachMyChild.Utilities;
using TeachMyChild.ViewModels;

namespace TeachMyChild.Controllers
{
    [Authorize]
    public class TutorController : BaseController
    {
        private ITutorProfileService tutorProfileService;
        private IAccountService actService;
        private IEducationEntityService eduService;
        private IWorkExperienceEntityService workExperienceEntityService;
        private ICourseService courseService;
        private ICourseCategoryService courseCategoryService;
        private IChatService chatService;
        IFileService fileService;
        IUnitOfWork uow;
        ApplicationUserManager userManager;
        public TutorController(ApplicationUserManager _userManager, ITutorProfileService _tutorProfileService, IAccountService _actService, IEducationEntityService _eduService,
            IWorkExperienceEntityService _workExperienceEntityService, ICourseService _courseservice, ICourseCategoryService _courseCategoryService, IChatService _chatService,
        IFileService _fileService, IUnitOfWork _uow)
        {
            uow = _uow;
            tutorProfileService = _tutorProfileService;
            actService = _actService;
            eduService = _eduService;
            workExperienceEntityService = _workExperienceEntityService;
            courseService = _courseservice;
            courseCategoryService = _courseCategoryService;
            chatService = _chatService;
            fileService = _fileService;
            userManager = _userManager;
        }
        [AllowAnonymous]
        // GET: Tutor
        public ActionResult Index()
        {
            ViewBag.DefaultTutorProfileImgUri = AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI;
            var allTutors = tutorProfileService.GetAllTutors(); // Todo: Get only completed profiles, not all profiles
            return View(allTutors);
        }

        // GET: Tutor/Create
        public ActionResult Create(string id)
        {
            return RedirectToAction("Step1", new { id = id });
            
        }


        // POST: Tutor/Create
      
        [HttpPost]
        public ActionResult Create(AddTutorViewModel model)
        {
            return View();      
        }

     
        public async Task<ActionResult> Step1(/*string id*/)
        {
            var user = await GetLoggedInUserAccount();
            if (!user.IsAccountComplete)
            {
                var nigeriaStates = P.GetStates("nigeria");
                ViewBag.States = nigeriaStates.AsEnumerable().Select(i => new SelectListItem { Text = i, Value = i });

                var userProfile = uow.tutorProfileDao.GetByUserAccountId(user.Id);
                ProfileBasics tutorModel = null;
                bool isEditing = true;
                if (userProfile == null)
                {
                    isEditing = false;
                    tutorModel = new ProfileBasics
                    {
                        AccountID = user.Id,
                        LastName = user.LastName,
                        FirstName = user.FirstName,                        
                        DateOfBirth = new DateTime(1985, 5, 5)    // just a default date to avoid error
                    };
                }
                else
                {
                    tutorModel = Mapper.Map<TutorProfile, ProfileBasics>(userProfile);
                    tutorModel.AccountID = user.Id;
                    tutorModel.ProfileID = userProfile.ID;
                }
                tutorModel.IsEditing = isEditing;

                ViewBag.AccountId = user.Id;
                return View(tutorModel);
            }
            // Account already complete, go to dashboard
            return RedirectToAction("Dashboard", new { id = user.Id });
        }

        // POST: Tutor/Create
        [HttpPost]
        public ActionResult Step1(ProfileBasics model)
        {
            var nigeriaStates = P.GetStates("nigeria");
            ViewBag.States = nigeriaStates.AsEnumerable().Select(i => new SelectListItem { Text = i, Value = i });
            if (!ModelState.IsValid)
            {
                // Todo: validate DateOfBirth from client side perhaps through Jquery
                ModelState.AddModelError("", "One or more fields has a wrong input");
                return View(model);
            }

            try
            {
                string userAccountId = model.AccountID;
                if (String.IsNullOrEmpty(userAccountId))
                {
                    ViewBag.ErrorMsg = "Unable to retrieve user account";
                    return View(model);
                }
                var userAct = actService.GetAccountById(userAccountId);
                if (userAct == null)
                {
                    ViewBag.ErrorMsg = "User account cannot be null";
                    return View(model);
                }
                
                if (model.IsEditing)
                {
                    var tutorModel = uow.tutorProfileDao.Get(model.ProfileID);
                    Mapper.Map(model, tutorModel);  // https://stackoverflow.com/a/25242322/7162741 suggested the fix for https://stackoverflow.com/questions/23201907/asp-net-mvc-attaching-an-entity-of-type-modelname-failed-because-another-ent caused by  // tutorModel = Mapper.Map<ProfileBasics, TutorProfile>(model);
                    tutorModel.LastStepCompleted = 1;
                    tutorProfileService.Update(tutorModel);
                }
                else
                {
                    var tutorModel = Mapper.Map<ProfileBasics, TutorProfile>(model);
                    tutorModel.Account = userAct;
                    tutorModel.LastStepCompleted = tutorModel.LastStepCompleted > 1 ? tutorModel.LastStepCompleted : 1;
                    tutorProfileService.Save(tutorModel, userAct);
                }
                return RedirectToAction("Step2", new { id = userAccountId });
            }
            catch (Exception ex)
            {
                ViewBag.ErrorMsg = "An error occured while saving user records" + ex.ToString();
                // Todo: log error
                return View();
            }
        }
        public async Task<ActionResult> Step2()
        {
            // in case someone trie to just step 1, first check if step 1 is completed, if not, redirect user to page 1.
            // do same for other steps as well
            var userAccount = await GetLoggedInUserAccount(); // userManager.FindByNameAsync(loggedInUsername);

            if (!userAccount.IsAccountComplete)
            {
                var userProfile = uow.tutorProfileDao.GetByUserAccountId(userAccount.Id);
                if (userProfile.LastStepCompleted < 1)
                    return RedirectToAction("Step1");   // must complete step 1 first
                EduAndProfessionalBackground tutorModel = null;
                bool isEditing = true;
                if (userProfile == null)
                {
                    tutorModel = new EduAndProfessionalBackground
                    {
                        AccountID = userAccount.Id,
                        Educations = new List<Education>(),
                        WorkExperiences = new List<WorkExperience>()
                    };
                }
                else
                {
                    tutorModel = Mapper.Map<TutorProfile, EduAndProfessionalBackground>(userProfile);

                    tutorModel.AccountID = userAccount.Id;
                    tutorModel.ProfileId = userProfile.ID;
                }
                tutorModel.IsEditing = isEditing;

                ViewBag.AccountId = userAccount.Id;
                return View(tutorModel);
            }
            return RedirectToAction("MyDashBoard");
            //return RedirectToAction("Login", "Account", new { msg = "Your profile setup is complete, you can now login" });
        }
        [HttpPost]
        public ActionResult Step2(EduAndProfessionalBackground model, string action)
        {
            if (ModelState.IsValid)
            {
                // For us to have reached step2, a tutor profile must have been created
                var profile = uow.tutorProfileDao.Get(model.ProfileId);
                if (profile == null)
                {
                    ModelState.AddModelError("", "user profile could not be found");
                    // Todo: log this
                    return View(model);
                }
                if (!model.Educations.Any())
                {
                    ModelState.AddModelError("", "Please add some education details");
                    // Todo: log this
                    return View(model);
                }

                // get the education and work experiences from the incoming model
                var newEdus = new List<EducationEntity>();
                foreach (var edu in model.Educations)
                {
                    newEdus.Add(new EducationEntity { InstitutionName = edu.InstitutionName, Concentration = edu.Concentration, DegreeAwarded = edu.DegreeAwarded, StartDate = edu.StartDate, EndDate = edu.EndDate, StillStudiesHere = edu.StillStudiesHere });
                }
                var newExperiences = new List<WorkExperienceEntity>();
                foreach (var exp in model.WorkExperiences)
                {
                    newExperiences.Add(new WorkExperienceEntity { CompanyName = exp.CompanyName, Description = exp.Description, JobTitle = exp.JobTitle, StartDate = exp.StartDate, EndDate = exp.EndDate, StillWorksHere = exp.StillWorksHere });
                }
                // delete the current experiences and add new ones
                if (profile.Educations != null && profile.Educations.Any())
                {
                    eduService.RemoveRange(profile.Educations);
                }
                if (profile.WorkExperiences != null && profile.WorkExperiences.Any())
                {
                    workExperienceEntityService.RemoveRange(profile.WorkExperiences);
                }

                profile.Educations = newEdus;
                profile.WorkExperiences = newExperiences;
                profile.LastStepCompleted = profile.LastStepCompleted > 2 ? profile.LastStepCompleted : 2;
                uow.tutorProfileDao.Update(profile);
                uow.Save();
               

                switch (action)
                {
                    case "Next":
                        {
                            return RedirectToAction("Step3", new { id = model.ProfileId });
                        }

                    case "Previous":
                        {
                            return RedirectToAction("Step1", new { id = model.ProfileId });
                        }
                    default:
                        break;
                }
            }
            if (model.Educations == null)
                model.Educations = new List<Education>();
            if (model.WorkExperiences == null)
                model.WorkExperiences = new List<WorkExperience>();
            ModelState.AddModelError("", "Please supply at least one education data");
            return View(model);
        }

        [ChildActionOnly]
        public PartialViewResult AlterEducationTable(EduAndProfessionalBackground model)
        {
            return PartialView("_EducationTable", model);
        }

        // GET
        public ActionResult Step3()
        {
            var theTutor = GetLoggedInTutorProfile();
            if (theTutor.LastStepCompleted < 2)
                return RedirectToAction("Step2");
            var courseCategories = courseCategoryService.GetAll().Where(c => c.Courses != null && c.Courses.Count > 0);         
           
            var model = new TutorCourseSelectionVM
            {
                HourlyRate = theTutor.HourlyRate,
                CourseCatModels = courseCategories.Select(cc => new CourseCatModel() { Name = cc.Name, ID = cc.ID, Courses = cc.Courses.Select(c => new CourseModel { ID = c.ID, Name = c.Name, Selected = theTutor.Courses.Contains(c) }).ToList() }).ToList()
            };
            return View(model);
        }


        // POST
        [HttpPost]
        public ActionResult Step3(TutorCourseSelectionVM model, string action)
        {
            if (ModelState.IsValid)
            {                
                try
                {
                    if(model.HourlyRate < AppConstants.MINIMUM_HOURLY_RATE || model.HourlyRate > AppConstants.MAXIMUM_HOURLY_RATE)
                    {
                        ModelState.AddModelError("", $"Hourly rate must be between {AppConstants.MINIMUM_HOURLY_RATE} and {AppConstants.MAXIMUM_HOURLY_RATE}");
                        return View(model);
                    }
                    var jaggedCourseIds = model.CourseCatModels.Where(c => c.Courses != null).Select(cc => cc.Courses.Where(c => c.Selected).Select(c => c.ID)).ToList();
                    List<long> courseIds = new List<long>();
                    // Just a max of 5 courses
                    foreach (var item in jaggedCourseIds)
                    {
                        courseIds.AddRange(item);
                    }
                    var selectedCourses = courseService.GetByIDs(courseIds);
                    if (selectedCourses.Count() == 0)
                    {
                        ModelState.AddModelError("", "Please select at least 1 course");
                        return View(model);
                    }
                    var theTutor = GetLoggedInTutorProfile();
                    theTutor.Courses.Clear();
                    theTutor.Courses.AddRange(selectedCourses);
                   
                    theTutor.HourlyRate = model.HourlyRate;
                    theTutor.LastStepCompleted = theTutor.LastStepCompleted > 3 ? theTutor.LastStepCompleted : 3;   // if the last step completed was 4 for example, then the user must have come back to only edit some details
                    tutorProfileService.Update(theTutor);
                    switch (action)
                    {
                        case "Next":
                            {
                                return RedirectToAction("Step4");
                            }

                        case "Previous":
                            {
                                return RedirectToAction("Step2");
                            }
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    // Todo: Log ex
                    ModelState.AddModelError("", "An unknown error occured!");
                    return View(model);
                }

            }
            return View(model);
        }
        // GET   Documents upload
        public async Task<ActionResult> Step4()
        {
            var userProfile = GetLoggedInTutorProfile();
            if (userProfile == null)
            {
                throw new Exception("User profile not found");
            }
            if (userProfile.LastStepCompleted < 3)
                return RedirectToAction("Step3");
            var userAccount = await GetLoggedInUserAccount();
            if(userAccount.IsAccountComplete)
                return RedirectToAction("Dashboard", new { id = userAccount.Id });
            ProfileDocuments profileDocs = LoadDocumentsForDisplay(userProfile);
            return View(profileDocs);
        }
        // POST
        [HttpPost]
        public ActionResult Step4(ProfileDocuments model, string action)
        {
            if (model.NonVidFiles == null || !model.NonVidFiles.Any())
            {
                ModelState.AddModelError("", "Please upload the required documents");
                return View(model);
            }

            try
            {
                var theTutor = GetLoggedInTutorProfile();
                string errorMessages = string.Empty;
                List<string> uploadedDocs = model.NonVidFiles.Select(f => f.DocumentType).ToList();
                if (ValidateUploadedDocuments(out errorMessages, uploadedDocs))
                {
                    theTutor.LastStepCompleted = theTutor.LastStepCompleted > 4 ? theTutor.LastStepCompleted : 4;
                    theTutor.Account.IsAccountComplete = true;
                    tutorProfileService.Update(theTutor); // Todo: confirm that the user account is also updated on the run

                    switch (action)
                    {
                        case "Complete Profile":
                            {
                                return RedirectToAction("MyDashboard");
                            }

                        case "Previous":
                            {
                                return RedirectToAction("Step3");
                            }
                        default:
                            return View(model);
                    }
                }

                else
                {
                    switch (action)
                    {
                        case "Complete Profile":
                            {
                                ModelState.AddModelError("", errorMessages);
                                return View(model);
                            }

                        case "Previous":
                            {
                                return RedirectToAction("Step3");
                            }
                        default:
                            return View(model);
                    }
                }
            }
            catch (Exception ex)
            {
                // Todo: log ex
                ModelState.AddModelError("", "An error occured and your request could not be processed");
                return View(model);
            }            
        }

        private bool ValidateUploadedDocuments(out string errorMessages, List<string> uploadedDocuments)
        {
            bool result = true;
            string errorMsg = string.Empty;
            List<string> requiredDocuments = AppConstants.REQUIRED_PROFILE_DOCUMENTS.Select(d => d.Value).ToList();
            foreach (var doc in requiredDocuments)
            {
                if(!uploadedDocuments.FindAll(s => s.Contains(doc, StringComparison.OrdinalIgnoreCase)).Any())
                {
                    result = false;
                    errorMsg += $"Please upload a {doc} \n";
                }
            }
            errorMessages = errorMsg;
            return result;
        }

        private static ProfileDocuments LoadDocumentsForDisplay(TutorProfile userProfile)
        {
            var userDocs = new List<NonVideoFile>();
            if (!String.IsNullOrEmpty(userProfile.ResumeUri))
            {
                string folder = $"~/assets/tutor_docs/{AppConstants.REQUIRED_PROFILE_DOCUMENTS[KeyRings.RESUME]}/";
                userDocs.Add(new NonVideoFile
                {
                    DocumentType = "Resume",
                    FileName = GetShortName(userProfile.ResumeUri.Replace(folder, string.Empty), 30)
                });
            }
            if (!String.IsNullOrEmpty(userProfile.UtilityBilUri))
            {
                string folder = $"~/assets/tutor_docs/{AppConstants.REQUIRED_PROFILE_DOCUMENTS[KeyRings.UTILITY_BILL]}/";
                userDocs.Add(new NonVideoFile
                {
                    DocumentType = "Utility Bill",
                    FileName = GetShortName(userProfile.UtilityBilUri.Replace(folder, string.Empty), 30)
                });
            }
          
            var profileDocs = new ProfileDocuments
            {
                NonVidFiles = userDocs,
                ProfileId = userProfile.ID
            };
            if (!String.IsNullOrEmpty(userProfile.ProfileImageUri))
            {
                profileDocs.ProfileImageUri = userProfile.ProfileImageUri;
            }
            else
            {
                profileDocs.ProfileImageUri = AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI;
            }
            return profileDocs;
        }
        // Todo: push this to a utility service
        private static string GetShortName(string text, int maxLength)
        {
            if (!(text.Length > maxLength))
            {
                return text;
            }
            int nPrefix = maxLength * 2 / 3;
            text = $"{text.Substring(0, nPrefix)}...{text.Substring(text.Length - (maxLength - nPrefix) + 3)}";
            return text;
        }


        //[ChildActionOnly]
        public ActionResult GetNonVidPartial(NonVideoFile model, string docName = null, string ProfileId = "")
        {
            // upload the docs one after the other
            if ((model.File == null || model.File.ContentLength < 1))
            {
                return new HttpStatusCodeResult(500, "No file selected!");                
            }

            string basePath = "~/assets/tutor_docs/" + docName;            
            try
            {
                var tutorProfile = uow.tutorProfileDao.Get(model.ProfileId);
                if (tutorProfile == null)
                    throw new Exception("The user profile cannot be found");
                var fileFormats = new[]
                 {
                    ".jpg", ".png", ".jpg", ".jpeg", ".pdf"
                  };
                var result = fileService.SaveFileToDisk(model.File, Server.MapPath(basePath), basePath, fileFormats);
                if (result["status"] == "error")
                {
                    return new HttpStatusCodeResult(500, result["msg"]);
                }
                string fileUri = result["msg"];
                DeleteDocument(tutorProfile, docName, fileUri);

                uow.tutorProfileDao.Update(tutorProfile);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            model.DocumentType = docName;

            return PartialView("_NonVideoFile", model);
        }

        public ActionResult UploadProfileImage(HttpPostedFileBase file)
        {
            if(file == null || file.ContentLength < 1)
                return new HttpStatusCodeResult(500, "No file selected!");
            string basePath = "~/assets/tutor_docs/" + "ProfileImages";
            try
            {
                var tutorProfile = GetLoggedInTutorProfile();
                if (tutorProfile == null)
                    return new HttpStatusCodeResult(500, "The user profile cannot be found");
                var fileFormats = new[]
                 {
                    ".jpg", ".png", ".jpg", ".jpeg", ".pdf"
                  };
                var result = fileService.SaveFileToDisk(file, Server.MapPath(basePath), basePath, fileFormats);
                if (result["status"] == "error")
                {
                    return new HttpStatusCodeResult(500, result["msg"]);
                }
                string fileUri = result["msg"];
                DeleteDocument(tutorProfile, "Profile Image", fileUri);
                tutorProfileService.Update(tutorProfile);
                return Json(new { msg = "File successfully updated!" });
            }
            catch (Exception ex)
            {
                // Todo: log error
                return Json(new { msg = "An error occured while updating image!" });
                //throw ex;
            }
        }

        public void DeleteProfileDocument(string profileId, string docType)
        {
            if (String.IsNullOrEmpty(profileId))
                throw new Exception("Profile Id cannot be null");
            try
            {
                long id = long.Parse(profileId);
                var tutorProfile = uow.tutorProfileDao.Get(id);
                if (tutorProfile == null)
                    throw new Exception("The user profile cannot be found");
                DeleteDocument(tutorProfile, docType.Trim(), null);
                uow.tutorProfileDao.Update(tutorProfile);
                uow.Save();

            }
            catch (Exception ex)
            {
                // Todo: log ex
                throw;
            }
        }

        private void DeleteDocument(TutorProfile tutorProfile, string docType, string newValue)
        {
            switch (docType)
            {
                case "Resume":
                    if (!String.IsNullOrEmpty(tutorProfile.ResumeUri))
                        RemoveFileFromDisk(tutorProfile.ResumeUri);
                    tutorProfile.ResumeUri = newValue;
                    break;
                case "Proof of Identification":
                    if (!String.IsNullOrEmpty(tutorProfile.IdentificationUri))
                        RemoveFileFromDisk(tutorProfile.IdentificationUri);
                    tutorProfile.IdentificationUri = newValue;
                    break;
                case "Utility Bill":
                    if (!String.IsNullOrEmpty(tutorProfile.UtilityBilUri))
                        RemoveFileFromDisk(tutorProfile.UtilityBilUri);
                    tutorProfile.UtilityBilUri = newValue;
                    break;
                case "Profile Image":
                    if (!String.IsNullOrEmpty(tutorProfile.ProfileImageUri))
                        RemoveFileFromDisk(tutorProfile.ProfileImageUri);
                    tutorProfile.ProfileImageUri = newValue;
                    break;
                case "Others":      // Todo: fix the below
                    if (!String.IsNullOrEmpty(tutorProfile.OtherFilesUris))
                        RemoveFileFromDisk(tutorProfile.OtherFilesUris);
                    tutorProfile.OtherFilesUris = tutorProfile.OtherFilesUris + "," + newValue;
                    break;
                default:
                    break;
            }

        }

        // Todo: remove this method
        public ActionResult Test(string id)
        {
            return View();
        }

        [HttpPost]
        public ActionResult Test(EduAndProfessionalBackground model, string action)
        {
            return View(model);
        }

        private void RemoveFileFromDisk(string resumeUri)
        {
            if (System.IO.File.Exists(Server.MapPath(resumeUri)))
            {
                System.IO.File.Delete(Server.MapPath(resumeUri));
            }
        }

        public PartialViewResult GetEduTableRow(Education model)
        {
            // model.Id = Guid.NewGuid();
            return PartialView("_TestEduTableList", model);
        }
        public PartialViewResult GetExpTableRow(WorkExperience model)
        {
            // model.Id = Guid.NewGuid();
            return PartialView("_ExperienceList", model);
        }
        

        [AllowAnonymous]
        public async Task<ActionResult> Dashboard(string id, string username = null)
        {
            var theProfileUser = actService.GetUserByUniqueProfileName(id);
            if(theProfileUser == null)      // attempt to view a non-existing account
            {
                TempData[KeyRings.ERROR_MESSAGE] = "Sorry, this user account does not exist";
                return RedirectToAction("Error");
            }
            var theTutor = tutorProfileService.GetProfileByUserAccountName(theProfileUser.UserName);
            var viewerName = User.Identity.Name;
            List<string> eduStr = new List<string>();
            if(theTutor.Educations != null && theTutor.Educations.Any())
            {
                foreach (var edu in theTutor.Educations)
                {
                    eduStr.Add($"{edu.DegreeAwarded} in {edu.Concentration} from {edu.InstitutionName}");
                }                
            }
            List<string> coursesStr = new List<string>();
            if (theTutor.Courses != null && theTutor.Courses.Any())
            {
                foreach (var course in theTutor.Courses)
                {
                    coursesStr.Add($"{course.Name}");
                }
            }
            ProfileVM model = new ProfileVM
            {
                FullName = theTutor.LastName + " " + theTutor.FirstName,
                City = theTutor.City ?? "",
                State = theTutor.State ?? "",
                About = theTutor.About ?? "",
                HourlyRate = theTutor.HourlyRate,
                Rating = (int)theTutor.AverageRating,
                ProfileImageImgUri = theTutor.ProfileImageUri ?? AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI,
                Educations = eduStr, 
                Courses = coursesStr
            };
            if (String.IsNullOrEmpty(viewerName)) // anonymous notLoggedIn user
            {
                ViewBag.ViewerType = "anonymous";
                return View(model);
            }
            else
            {                
                if (String.Compare(viewerName, theProfileUser.UserName, true) == 0)     // user viewing his own account
                {
                    ViewBag.ViewerType = "self";
                    var theLoggedInTutor = GetLoggedInTutorProfile();
                    int lastStepCompleted = theLoggedInTutor.LastStepCompleted ;
                    string navigateUrl = string.Empty;
                    switch (lastStepCompleted)
                    {
                        case 0:
                            navigateUrl = "step1";
                            break;
                        case 1:
                            navigateUrl = "step2";
                            break;
                        case 2:
                            navigateUrl = "step3";
                            break;
                        case 3:
                            navigateUrl = "step4";
                            break;
                        default:
                            break;
                    }
                    ViewBag.SetupPage = navigateUrl;
                    return View(model);
                }
                else
                {
                    var theViewer = actService.GetAccountByUsername(viewerName);
                    if (await userManager.IsInRoleAsync(theViewer.Id, "Parent")){  // viewers who are Parents (the only ones who can message the tutors)
                        ViewBag.ViewerType = "parent";
                        return View(model);
                    }
                    else
                    {
                        ViewBag.ViewerType = "other";
                        return View(model);
                    }
                }
            }            
        }      
     
        //[Authorize(Roles =AppConstants.TUTOR_ROLE_NAME+","+AppConstants.PARENT_ROLE_NAME)]
        public async Task<ActionResult> MyDashBoard()
        {            
            string username = User.Identity.Name;
            var userAccount = await userManager.FindByNameAsync(username);
            if (await userManager.IsInRoleAsync(userAccount.Id, AppConstants.PARENT_ROLE_NAME))
                return RedirectToAction("Dashboard", "Parent");

            if (!await userManager.IsInRoleAsync(userAccount.Id, AppConstants.TUTOR_ROLE_NAME))
                return RedirectToAction("Login", "Home");
            var userProfile = uow.tutorProfileDao.GetByUserAccountId(userAccount.Id);
            // Todo: rename the viewBag below
            ViewBag.userId = userAccount.ProfileUniqueName; //   userProfile?.ProfileUniqueName?? ""; // userAccount.Id;
            if(userProfile == null)
            {

            }
            //DashboardViewModel model1 = new DashboardViewModel
            //{
            //    FullName = $"{userAccount.FirstName} {userAccount.LastName?? ""}",
            //    Email = userAccount.Email,
            //    YearJoined =userAccount.DateCreated.Year.ToString(),
            //    AverageRating = 0,// userProfile.AverageRating,
            //    Reviews = userProfile.Reviews == null ? new List<Review>() : userProfile.Reviews.OrderByDescending(r => r.DateCreated).Take(5).ToList(),
            //    OngoingTutorials = userProfile.OngoingTutorials == null ? new List<TutorialRequest>() : userProfile.OngoingTutorials.OrderByDescending(r => r.DateCreated).ToList(),
            //    CompletedTutorials = userProfile.OngoingTutorials == null ? 0 : userProfile.CompletedTutorials.Count, 
            //};
            List<TutorialRequest> ongoingTutorials = userProfile.Tutorials == null ? new List<TutorialRequest>() : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Started).OrderByDescending(r => r.DateCreated).ToList();
            int numberOfCompletedTutorials = userProfile.Tutorials == null ? 0 : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Completed).Count();

            DashboardViewModel model = new DashboardViewModel();
            model.FullName = $"{userAccount.FirstName} {userAccount.LastName ?? ""}";
            model.Email = userAccount.Email;
            model.MonthJoined = DateHelper.GetMonthName(userAccount.DateCreated.ToLocalTime().Month);
            model.YearJoined = userAccount.DateCreated.ToLocalTime().Year.ToString();
            model.AverageRating = userProfile.AverageRating;
            model.Reviews = userProfile.Reviews == null ? new List<Review>() : userProfile.Reviews.OrderByDescending(r => r.DateCreated).Take(5).ToList();
            model.OngoingTutorials = ongoingTutorials; // userProfile.OngoingTutorials == null ? new List<TutorialRequest>() : userProfile.OngoingTutorials.OrderByDescending(r => r.DateCreated).ToList();
            model.CompletedTutorials = numberOfCompletedTutorials;
            model.ProfileImageUri = userProfile.ProfileImageUri == null ? AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI : userProfile.ProfileImageUri;
            return View(model);
        }

        public ActionResult Inbox(string userToMsgId = null)
        {
            return RedirectToAction("Index");
            //var userProfile = GetLoggedInTutorProfile();
            //List<TutorialRequest> ongoingTutorials = userProfile.Tutorials == null ? new List<TutorialRequest>() : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Started).OrderByDescending(r => r.DateCreated).ToList();
            //int numberOfCompletedTutorials = userProfile.Tutorials == null ? 0 : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Completed).Count();

            //ProfileHeaderSectionVM sectionModel = new ProfileHeaderSectionVM();
            //sectionModel.FullName = $"{userProfile.FirstName} {userProfile.LastName ?? ""}";
            //sectionModel.Email = userProfile.Email;
            //sectionModel.MonthJoined = DateHelper.GetMonthName(userProfile.Account.DateCreated.ToLocalTime().Month);
            //sectionModel.YearJoined = userProfile.Account.DateCreated.ToLocalTime().Year.ToString();
            //sectionModel.NumberOfOngoingTutorials = ongoingTutorials.Count; // userProfile.OngoingTutorials == null ? new List<TutorialRequest>() : userProfile.OngoingTutorials.OrderByDescending(r => r.DateCreated).ToList();
            //sectionModel.NumberOfCompletedTutorials = numberOfCompletedTutorials;
            //sectionModel.ProfileImageUri = userProfile.ProfileImageUri == null ? AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI : userProfile.ProfileImageUri;

            var theUserId = "";// userProfile.Account.Id;
            // Get all the chats this user has
            List<Chat> myChats = chatService.GetAllChatsForUser(theUserId).OrderByDescending(c=>c.DateModified).ToList();  // new List<Chat>(); // chats with this user
            // Get the list of users this logged in user has chated with. This is to be displayed in the left sidebar
            var usersInChat = myChats.Select(c => new ChatInboxUserModel { Id = c.User1Id == theUserId ? c.User2Id : c.User1Id }).ToList();
            foreach (var userModel in usersInChat)
            {
                // userModel.UserName = userProfile.LastName + " " + userProfile.FirstName;
                // userModel.AvatarUri = tutorProfileService.GetProfileImageUriForTutor(userProfile.ID);
            }

            string nameOfUserInChat = string.Empty;

            // Get the messages to be rendered in the chat body, this is usually the messages of the most recent chat or..
            // the messages of a user which has just been selected to be chatted with perhaps from the Tutors' list
            var defaultMessages = new List<Message>();           
            if (!String.IsNullOrEmpty(userToMsgId))
            {
                // Check if this user has an existing chat with the logged in user
                if(myChats.Any(c => c.User1Id == userToMsgId || c.User2Id == userToMsgId))
                {
                    // there's an existing chat
                    defaultMessages.AddRange(myChats.First((c => c.User1Id == userToMsgId || c.User2Id == userToMsgId)).Messages);
                    nameOfUserInChat = "An existing user selected";
                    // 
                }
                else
                {
                    // show the name of the user in the left sidebar by adding it to the list
                    nameOfUserInChat = "An new user selected";
                }
            }
            else // no particular user has been selected to chat with, load the messages of the most recently updated chat
            {
                if (myChats.Any())
                {
                    defaultMessages.AddRange(myChats.First().Messages.OrderBy(m => m.DateCreated));
                }
                nameOfUserInChat = "An existing user loaded";
            }

            TutorInboxModel model = new TutorInboxModel
            {
               // ProfileHeader = sectionModel, 
                UsersInChat = usersInChat,
                ChatBody = usersInChat.Any() ? new ChatBodyModel { TheUserId =  theUserId, ChatPartnerId = usersInChat.First().Id,  Messages = defaultMessages} :  new ChatBodyModel()
            };

            ViewBag.UserBeingChattedWith = nameOfUserInChat;
            return View(model);
        }

        public ActionResult Notification()
        {
            var userProfile = GetLoggedInTutorProfile();          
            List<TutorialRequest> ongoingTutorials = userProfile.Tutorials == null ? new List<TutorialRequest>() : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Started).OrderByDescending(r => r.DateCreated).ToList();
            int numberOfCompletedTutorials = userProfile.Tutorials == null ? 0 : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Completed).Count();

            ProfileHeaderSectionVM sectionModel = new ProfileHeaderSectionVM();
            sectionModel.FullName = $"{userProfile.FirstName} {userProfile.LastName ?? ""}";
            sectionModel.Email = userProfile.Email;
            sectionModel.MonthJoined = DateHelper.GetMonthName(userProfile.Account.DateCreated.ToLocalTime().Month);
            sectionModel.YearJoined = userProfile.Account.DateCreated.ToLocalTime().Year.ToString();
            sectionModel.NumberOfOngoingTutorials = ongoingTutorials.Count;
            sectionModel.NumberOfCompletedTutorials = numberOfCompletedTutorials;
            sectionModel.ProfileImageUri = userProfile.ProfileImageUri == null ? AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI : userProfile.ProfileImageUri;
            NotificationModel model = new NotificationModel
            {
                ProfileHeader = sectionModel
            };
            return View(model);            
        }
        public ActionResult Withdraw()
        {
            var userProfile = GetLoggedInTutorProfile();
            List<TutorialRequest> ongoingTutorials = userProfile.Tutorials == null ? new List<TutorialRequest>() : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Started).OrderByDescending(r => r.DateCreated).ToList();
            int numberOfCompletedTutorials = userProfile.Tutorials == null ? 0 : userProfile.Tutorials.Where(t => t.Status == Core.Enums.TutorialStatus.Completed).Count();

            ProfileHeaderSectionVM sectionModel = new ProfileHeaderSectionVM();
            sectionModel.FullName = $"{userProfile.FirstName} {userProfile.LastName ?? ""}";
            sectionModel.Email = userProfile.Email;
            sectionModel.MonthJoined = DateHelper.GetMonthName(userProfile.Account.DateCreated.ToLocalTime().Month);
            sectionModel.YearJoined = userProfile.Account.DateCreated.ToLocalTime().Year.ToString();
            sectionModel.NumberOfOngoingTutorials = ongoingTutorials.Count;
            sectionModel.NumberOfCompletedTutorials = numberOfCompletedTutorials;
            sectionModel.ProfileImageUri = userProfile.ProfileImageUri == null ? AppConstants.DEFAULT_TUTOR_PROFILE_IMAGE_URI : userProfile.ProfileImageUri;
            WithdrawModel model = new WithdrawModel
            {
                ProfileHeader = sectionModel
            };
            return View(model);
        }
        private TutorProfile GetLoggedInTutorProfile()
        {
            var username = User.Identity.Name;
            if (String.IsNullOrEmpty(username))
                return null;
            return tutorProfileService.GetProfileByUserAccountName(username);
        }

        private async Task<ApplicationUser> GetLoggedInUserAccount()
        {
            return await userManager.FindByNameAsync(User.Identity.Name);
        }
    }
}
