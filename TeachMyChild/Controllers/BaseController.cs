﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachMyChild.Helpers;

namespace TeachMyChild.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        public ActionResult Error(string errorMsg = "")
        {
            if (String.IsNullOrEmpty(errorMsg) && String.IsNullOrWhiteSpace(errorMsg))
                errorMsg = "An error occured while processing your request";
            string msg = TempData[KeyRings.ERROR_MESSAGE]?.ToString();
            if (String.IsNullOrEmpty(msg))
            {
                ViewBag.ErrorMsg = errorMsg;
            }
            else
            {
                ViewBag.ErrorMsg = msg;
                TempData.Remove(KeyRings.ERROR_MESSAGE);
            }
            return View();
        }
    }
}