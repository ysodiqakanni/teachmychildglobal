﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Data;
using TeachMyChild.Data.Contracts;
using TeachMyChild.Helpers;
using TeachMyChild.Logic.Contracts;

namespace TeachMyChild.Controllers
{
    [Authorize(Roles = AppConstants.ADMIN_ROLE_NAME)]
    public class CoursesController : Controller
    {
        IUnitOfWork uow;
        ICourseService courseService;
        ICourseCategoryService courseCatService;
        public CoursesController(IUnitOfWork _uow, ICourseService _courseService, ICourseCategoryService _courseCatService)
        {
            uow = _uow;
            courseService = _courseService;
            courseCatService = _courseCatService;
        }
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Courses
        [AllowAnonymous]
        public ActionResult Index()
        {
            var alCourses = courseService.GetAll();
            return View(alCourses);
        }

        // GET: Courses/Details/5
        public ActionResult Details(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = courseService.GetById(id.Value);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // GET: Courses/Create
        public ActionResult Create()
        {
            ViewBag.CategoryId = courseCatService.GetAll().AsQueryable().Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() });
            return View();
        }

        // POST: Courses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Course course, string CategoryId)
        {
            ViewBag.CategoryId = courseCatService.GetAll().AsQueryable().Select(c => new SelectListItem { Text = c.Name, Value = c.ID.ToString() });
            if (ModelState.IsValid)
            {
                try
                {

                    if (courseService.CourseWithNameAlreadyExists(course.Name, course.CategoryId))
                    {
                        ModelState.AddModelError("", "A course with this name already exists under the selected category");
                        return View(course);
                    }
                    courseService.Save(course);
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    // Todo: Log ex
                    ModelState.AddModelError("", "An error occured while saving the course");
                    return View(course);
                }
            }            
            return View(course);
        }

        // GET: Courses/Edit/5
        public ActionResult Edit(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Name,Description,DateCreated,DateModified")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Entry(course).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(course);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Courses.Find(id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(long id)
        {
            Course course = db.Courses.Find(id);
            db.Courses.Remove(course);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        public ActionResult Categories()
        {
            var categories = courseCatService.GetAll();
            return View(categories);
        }
        public ActionResult AddCategory()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddCategory(CourseCategory model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    courseCatService.CreateNewCategory(model);
                    return RedirectToAction("Categories");
                }
                catch (Exception ex)
                {
                    // Log ex
                    ModelState.AddModelError("", "An error occured while saving category");
                    return View();
                }
            }
            return View(model);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
