﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TeachMyChild.Startup))]
namespace TeachMyChild
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
