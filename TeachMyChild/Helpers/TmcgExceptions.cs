﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachMyChild.Helpers
{
    // Todo: Move to a separate project
    public class TmcgException : Exception
    {
        public TmcgException()
        {
        }

        public TmcgException(string message)
        : base(message)
        {
        }

        public TmcgException(string message, Exception inner)
        : base(message, inner)
        {
        }
    }
}