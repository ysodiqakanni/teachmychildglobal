﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace TeachMyChild.Helpers
{
    public static class AppConstants
    {
        public const string TUTOR_ROLE_NAME = "Tutor";
        public const string PARENT_ROLE_NAME = "Parent";
        public const string ADMIN_ROLE_NAME = "Parent";

        public const string DEFAULT_TUTOR_PROFILE_IMAGE_URI = "/assets/img/avatar.png";
        public const decimal MINIMUM_HOURLY_RATE = 1000;
        public const decimal MAXIMUM_HOURLY_RATE = 10000;
        public const string DESTINATION_EMAIL_FOR_GENERAL_CONTACT = "teachmychildglobal@gmail.com";

        public static readonly List<string> RequiredProfileDocuments1 = new List<string>() { "s" };
        public static readonly Dictionary<string, string> REQUIRED_PROFILE_DOCUMENTS = new Dictionary<string, string>() { { KeyRings.RESUME, "Resume" }, { KeyRings.UTILITY_BILL, "Utility Bill" }, { KeyRings.IDENTIFICATION, "Identification" } };

        public static readonly NetworkCredential MAIL_CREDENTIALS = new NetworkCredential(
                               ConfigurationManager.AppSettings["mailAccount"],
                               ConfigurationManager.AppSettings["mailPassword"]
                               );
    }
    public class KeyRings
    {
        public static string ERROR_MESSAGE = "ErrorMessage";
        public static string RESUME = "Resume";
        public static string UTILITY_BILL = "UtilityBill";
        public static string IDENTIFICATION = "Identification";
    }
}