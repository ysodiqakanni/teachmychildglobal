﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachMyChild.Helpers
{
    public class AvailableCourses
    {
        public static Dictionary<string, List<string>> AllCourses = new Dictionary<string, List<string>>
        {
            { "Primary", new List<string>
                {
                    "Mathematics", "English Language", "Basic Science and Technology", "Christian Religious Knowledge", "Islamic Studies"
                }
            },
            { "Junior Secondary", new List<string>
                {
                    "Mathematics", "English Language", "Basic Science and Technology", "IT", "Civic edu", "French"
                }
            },
            { "Senior Secondary", new List<string>
                {
                    "Mathematics", "English Language", "Physics", "Chemistry", "Biology", "Further Maths", "Accounting", "Commerce", "Government", "Geography", "Basic Electronics", "Technical Drawings"
                }
            },
            { "Foreign Languages", new List<string>
                {
                    "Spanish", "German", "French", "Mandarin", "Arabic", "Italiano", "Russian", "Portuguese"
                }
            },
            { "Advanced1Tech", new List<string>
                {
                    "Computer Programming", "Microsoft Office", "Data Structures and Algorithms", "Machine Learning", "Artificial Intelligence", "Deep Learning", "Digital Marketting", "Desktop Publishing", "Embedded Systems", "AutoCAD", "UI/UX", "Graphics design"
                }
            },
            {
                "Advanced2Maths", new List<string>
                {
                    "Computational Methods", "Statistics and Probability", "Calculus", "Fourier Series & Transform", "Laplace transform"
                }
            }
        };
    }
}