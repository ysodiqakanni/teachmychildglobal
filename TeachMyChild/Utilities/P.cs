﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachMyChild.Utilities
{
    public class P
    {
        public static List<string> GetStates(string country)
        {
            var result = new List<string>();
            if (String.Compare(country.ToLower(), "nigeria") == 0)
            {
                result = new List<string>
                {"Abia",
                  "Adamawa",
                  "Anambra",
                  "Akwa Ibom",
                  "Bauchi",
                  "Bayelsa",
                  "Benue",
                  "Borno",
                  "Cross River",
                  "Delta",
                  "Ebonyi",
                  "Enugu",
                  "Edo",
                  "Ekiti",
                  "FCT",
                  "Gombe",
                  "Imo",
                  "Jigawa",
                  "Kaduna",
                  "Kano",
                  "Katsina",
                  "Kebbi",
                  "Kogi",
                  "Kwara",
                  "Lagos",
                  "Nasarawa",
                  "Niger",
                  "Ogun",
                  "Ondo",
                  "Osun",
                  "Oyo",
                  "Plateau",
                  "Rivers",
                  "Sokoto",
                  "Taraba",
                  "Yobe",
                  "Zamfara",
                };
            }
            return result;
        }
    }
}