﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using TeachMyChild.Core.Implementations;
using TeachMyChild.Helpers;

namespace TeachMyChild.ViewModels
{
    public class AddTutorViewModel
    {
        public string AccountID { get; set; }
        public long ID { get; set; }        // this is the profile ID

        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Name can only contain letters, space, hyphens and numbers"), Display(Name = "Last Name")]
        public string LastName { get; set; }

        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Name can only contain letters, space, hyphens and numbers"), Display(Name = "First Name")]
        public string FirstName { get; set; }

        [DataType(DataType.Date), Display(Name ="Date of birth")]
        public DateTime DateOfBirth { get; set; }

        [DataType(DataType.MultilineText)]
        public string About { get; set; }

        [DataType(DataType.PhoneNumber, ErrorMessage ="Phone number has a wrong format")]
        public string Phone { get; set; }

        [RegularExpression(@"^[ a-zA-Z0-9_,]+", ErrorMessage ="Invalid address")]
        public string HomeAddress { get; set; }

        public string LinkedInUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string FacebookUrl { get; set; }
    }
    public class ProfileBasics
    {
        public string AccountID { get; set; }
        public bool IsEditing { get; set; }
        public long ProfileID { get; set; }        // this is the profile ID

        [Required(ErrorMessage ="Last name is required")]
        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Name can only contain letters, space, hyphens and numbers"), Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "First name is required")]
        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Name can only contain letters, space, hyphens and numbers"), Display(Name = "First Name")]
        public string FirstName { get; set; }
        
        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Name can only contain letters, space, hyphens and numbers"), Display(Name = "Middle Name")]
        public string MiddleName { get; set; }

        [Required]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Date of birth")]
        public DateTime DateOfBirth { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        public string About { get; set; }

        [Required]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Phone number has a wrong format")]
        public string Phone { get; set; }

        [Required(ErrorMessage ="Address is required")]
        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Invalid address"), Display(Name ="Address 1"), DataType(DataType.Text)]
        public string HomeAddressLine1 { get; set; }

        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Invalid address"), Display(Name = "Address 2"), DataType(DataType.Text)]
        public string HomeAddressLine2 { get; set; }

        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Invalid address"), Display(Name = "Address 3"), DataType(DataType.Text)]
        public string HomeAddressLine3 { get; set; }

        [Required]
        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Invalid City Name"), Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [RegularExpression(@"^[ a-zA-Z0-9_]+", ErrorMessage = "Invalid State Name"), Display(Name = "State")]
        public string State { get; set; }

        [DataType(DataType.PostalCode), Display(Name ="Zip or Postal code")]
        public string ZipCode { get; set; }
        public string LinkedInUrl { get; set; }
        public string TwitterUrl { get; set; }
        public string FacebookUrl { get; set; }
    }
    
    [MaxFileSize(1 * 1024 * 1024, ErrorMessage = "Maximum allowed file size is {0} bytes")]     // 1MB
    public class ProfileDocuments
    {
        public string AccountID { get; set; }
        public long ProfileId { get; set; }
        public string ProfileImageUri { get; set; }
        public List<NonVideoFile> NonVidFiles { get; set; }
    }
    public class NonVideoFile
    {
        public long ProfileId { get; set; }
        public string FileName { get; set; }
        public string DocumentType { get; set; }

        [Display(Name = "Non-video file")]
        public HttpPostedFileBase File { get; set; }
    }
    public class EduAndProfessionalBackground
    {
        public long ProfileId { get; set; }
        public string AccountID { get; set; }
        public bool IsEditing { get; set; }
        public List<WorkExperience> WorkExperiences { get; set; }

        [Required(ErrorMessage ="You must add at least one education info")]
        public List<Education> Educations { get; set; }
        public List<ProfessionalCertificate> ProfessionalCertificates { get; set; }
        public List<Award> Awards { get; set; }
    }

    public class Award
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class ProfessionalCertificate
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class WorkExperience
    {
        public Guid Id { get; set; }
        [Required, Display(Name ="Company Name")]
        public string CompanyName { get; set; }

        [Required, Display(Name = "JOb Title")]
        public string JobTitle { get; set; }


        public string Description { get; set; }

        [Required, Display(Name = "Start Date")]
        public DateTime StartDate { get; set; } = DateTime.Now.AddYears(-15);

        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; } = DateTime.Now.AddYears(-5);

        public bool StillWorksHere { get; set; } = false;
    }
    public class Education
    {
        //public Guid Id { get; set; }
        [Required, Display(Name = "Institution Name")]
        public string InstitutionName { get; set; }

        [Required]
        public string Concentration { get; set; }

        [Required, Display(Name = "Degree Awarded")]
        public string DegreeAwarded { get; set; }

        [Required, Display(Name = "Start Date")]
        public DateTime StartDate { get; set; } = DateTime.Now.AddYears(-15);

        [Display(Name = "End Date")]
        public DateTime EndDate { get; set; } = DateTime.Now.AddYears(-5);

        public bool StillStudiesHere { get; set; } = false;
    }

    public class DashboardViewModel
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string MonthJoined { get; set; }
        public string YearJoined { get; set; }
        public int CompletedTutorials { get; set; }
        public decimal TotalEarnings { get; set; }
        public decimal AvailableToWithdraw { get; set; }
        public string ProfileImageUri { get; set; }
        public double AverageRating { get; set; }

        public List<TutorialRequest> OngoingTutorials { get; set; }
        public virtual List<Review> Reviews { get; set; }
    } 


    public class TutorCourseSelectionVM
    {
        [Display(Name ="Hourly Rate")]
        [DataType(DataType.Currency)]
        public decimal HourlyRate { get; set; }

        public List<CourseCatModel> CourseCatModels { get; set; }
    }

    public class CourseCatModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public List<CourseModel> Courses { get; set; }
    }
    public class CourseModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
    public class TutorInboxModel
    {
        public string UserAccountId { get; set; }
        public ProfileHeaderSectionVM ProfileHeader { get; set; }
        public List<ChatInboxUserModel> UsersInChat { get; set; }
        public ChatBodyModel ChatBody { get; set; }
    }
    public class ChatBodyModel
    {
        public string TheUserId { get; set; }
        public string ChatPartnerId { get; set; }
        public List<Message> Messages { get; set; }
    }
    public class ChatInboxUserModel
    {
        public string Id { get; set; }
        public string AvatarUri { get; set; }
        public UserChatStatus Status { get; set; }
        public string UserName { get; set; }
        public string Notification { get; set; }  // Last seen, x new message, typing... etc
    }
    public enum UserChatStatus
    {
        Offline, Online, Busy
    }

    public class NotificationModel
    {
        public ProfileHeaderSectionVM ProfileHeader { get; set; }
        
    }
    public class WithdrawModel
    {
        public ProfileHeaderSectionVM ProfileHeader { get; set; }
    }
    public class ProfileHeaderSectionVM
    {
        public string FullName { get; set; }
        public string Email { get; set; }
        public string MonthJoined { get; set; }
        public string YearJoined { get; set; }
        public string ProfileImageUri { get; set; }
        public int AverageRating { get; set; }
        public int NumberOfCompletedTutorials { get; set; }
        public int NumberOfOngoingTutorials { get; set; }
        public decimal TotalEarnings { get; set; }
        public decimal AvailableToWithdraw { get; set; }
    }
    public class ProfileVM
    {
        public string FullName { get; set; }
        public string About { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ProfileImageImgUri { get; set; }
        public int Rating { get; set; }
        public decimal HourlyRate { get; set; }
        public List<string> Educations { get; set; }
        public List<string> Courses { get; set; }
    }
}