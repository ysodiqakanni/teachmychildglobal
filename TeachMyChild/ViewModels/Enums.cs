﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachMyChild.ViewModels
{
    public enum AccountType
    {
        None = 0, Tutor, Parent, Admin
    }
}