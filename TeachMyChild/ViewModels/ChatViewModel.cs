﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TeachMyChild.ViewModels
{
    public class user
    {
        //id, name, type, fontName, fontSize, fontColor, sex, age, friendsList, status, memberType

        public string ConnectionId { get; set; }
        public int id { get; set; }
        public string name { get; set; }
        public List<user> friendsList { get; set; }
        public string fontName { get; set; }
        public string fontSize { get; set; }
        public string fontColor { get; set; }
        public string sex { get; set; }
        public int age { get; set; }
        public string status { get; set; }
        public string memberType { get; set; }
        public string avator { get; set; }
        public string ContextName { get; set; }
    }
    public enum Status
    {
        Online,
        Away,
        Busy,
        Offline
    }
}