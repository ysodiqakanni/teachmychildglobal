﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TeachMyChild.Core.Implementations;

namespace TeachMyChild.ViewModels
{
    public class CreateNewTutorialRequestVM
    {
        [Required]
        // start and end time eg 9am to 11am, how long the contract would be
        public string ShortSummary { get; set; }
        public string Description { get; set; }
        public int NumberOfHoursInADay { get; set; }
        public int NumberOfDaysInAWeeek { get; set; }
        public string TimeOfDay { get; set; }
        [Required]
        [DataType(DataType.Currency)]
        [Display(Name ="Budget")]
        public decimal ProposedChargePerHour { get; set; }

        public string StartTime { get; set; } // eg 9am
        public string EndTime { get; set; } // eg 11am

        public int NumberOfStudents { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string State { get; set; }

        public Course CourseToLearn { get; set; }

        public Course[] Courses { get; set; }
        public IEnumerable<SelectListItem> CourseListItems { get; set; }
    }
    public class PlaceBidVM
    {
        public long TutorialRequestId { get; set; }
        public string Amount { get; set; }
        public string Proposal { get; set; }
    }
}