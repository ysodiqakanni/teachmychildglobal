﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TeachMyChild.ViewModels
{
    public class IndexViewModel
    {
        public List<TutorModel> featuredTutors { get; set; }
    }
    public class TutorModel
    {
        public string ProfileImageUri { get; set; }
        public string UniqueName { get; set; }
        public string FullName { get; set; }
        public string Subjects { get; set; }
    }
    public class ContactViewModel
    {
        public string From { get; set; }
        public string To { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public string Name { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
    }
}