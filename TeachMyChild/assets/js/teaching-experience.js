﻿var totalChecked = 0;
window.onload = function(e){
    var count = $("[type='checkbox']:checked").length;
    totalChecked = count;
    if (totalChecked >= 5) {
        var uncheckedCourses = $('.uncheckedCourses');
        uncheckedCourses.attr("disabled", true);
    }
}
function chkBoxChanged(el) {
    var chk = $(el);
    if (chk.is(":checked")) {
        chk.addClass("checkedCourses").removeClass('uncheckedCourses');
        totalChecked += 1;
        if (totalChecked >= 5) {            
            var uncheckedCourses = $('.uncheckedCourses');
            uncheckedCourses.attr("disabled", true);
        }
    }
    else {
        chk.addClass("uncheckedCourses").removeClass('checkedCourses');
        totalChecked -= 1;
        if (totalChecked == 4) {
            var uncheckedCourses = $('.uncheckedCourses');
            uncheckedCourses.removeAttr("disabled");
        }
    }
    
  
}