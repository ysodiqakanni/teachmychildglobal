﻿// debugger
var itemCountEdu = 0;
var itemCountExp = 0;

//$(function () {
//    $("#addEduRow").click(function (e) {
//        itemCountEdu++;
//        addEducation();
//    })

//    $("#addExpRow").click(function (e) {
//        itemCountExp++;
//        addExperience();
//    })

//});
$(document).on("click", "#btnDeleteEdu", function () {
    if (confirm('Are you sure you want to remove this education?')) {
        itemCountEdu--;
        $(this).closest("tr").remove();
    }

});
$(document).on("click", "#btnDeleteExp", function () {
    if (confirm('Are you sure you want to remove this experience?')) {
        itemCountExp--;
        $(this).closest("tr").remove();
    }
  
});



function createNewItemEdu(education) {

    $('#table_edu').append(education);

};

function addEducation(eduList) { 
    //////
    var url = "/Tutor/GetEduTableRow";
    //var theTable = $('#table_edu');
    var theDiv = $('#eduTable'); 
    $.ajax({
        type: "POST",
        url: url,
        data: JSON.stringify(eduList),  
        contentType: "application/json",
        processData: false,

        success: function (response) {
            alert("success")
            //var tableData = theTable.html();
            //theTable.html(tableData + response);
            theDiv.html(response);
        },
        error: function () {
            alert("An error occured");
        },

    })
  


//    var eduTableRowCount = document.getElementById("table_edu").getElementsByTagName("tr").length;
//    eduTableRowCount = eduTableRowCount - 1;
//    if ((instiName && concenatration && degreeaward && start) && (end || stillStudiesHere)) {
//        var newEduItem = '<tr style="font-size:12px">' +
//               '<td>' + itemCountEdu + '</td>' +
//               '<td>' + instiName + '<input data-val="true" data-val-required="The Institution Name field is required." id="Educations_' + eduTableRowCount + '__InstitutionName" name="Educations[' + eduTableRowCount + '].InstitutionName" type="hidden" value=' + instiName +'>' + '</td>' +
//               '<td>' + concenatration + '<input data-val="true" data-val-required="The Concentration field is required." id="Educations_' + eduTableRowCount + '__Concentration" name="Educations[' + eduTableRowCount + '].Concentration" type="hidden" value=' + concenatration + '>' + '</td>' +
//               '<td>' + degreeaward + '<input data-val="true" data-val-required="The Degree Awarded field is required." id="Educations_' + eduTableRowCount + '__DegreeAwarded" name="Educations[' + eduTableRowCount + '].DegreeAwarded" type="hidden" value=' + degreeaward + '>' + '</td>' +
//               '<td>' + start + '<input data-val="true" data-val-date="The field Start Date must be a date." data-val-required="The Start Date field is required." id="Educations_' + eduTableRowCount + '__StartDate" name="Educations[' + eduTableRowCount + '].StartDate" type="hidden" value=' + start + '>' + '<input data-val="true" data-val-required="The StillStudiesHere field is required." id="Educations_' + eduTableRowCount + '__StillStudiesHere" name="Educations[' + eduTableRowCount + '].StillStudiesHere" type="hidden" value='+stillStudiesHere+'>' + '</td>' +
//               '<td>' + endVal + '<input data-val="true" data-val-date="The field End Date must be a date." data-val-required="The End Date field is required." id="Educations_' + eduTableRowCount + '__EndDate" name="Educations[' + eduTableRowCount + '].EndDate" type="hidden" value=' + end + '>' + '<input data-val="true" data-val-required="The StillStudiesHere field is required." id="Educations_' + eduTableRowCount + '__StillStudiesHere" name="Educations[' + eduTableRowCount + '].StillStudiesHere" type="hidden" value=' + stillStudiesHere + '>' + '</td>' +
//               '<td style="width:100px;text-align:center; padding:5px">' + '<button class="delete btn btn-primary btn-xs my-xs-btn" type="button" id="btnDeleteEdu" ">'
//+ '<span class="glyphicon glyphicon-trash"></span> Delete </button>' + '</td>';
//        createNewItemEdu(newEduItem);
//        clearEducationInputFields()
//    }
//    else {
//        alert("Please enter required education data");
//    }
};

function createNewItemExp(experience) {

    $('#table_exp').append(experience);

};

function addExperience() {
    //FOR CLARITY, GRAB THE INPUTTED TEXT AND STORE IT IN A VAR

    var company = $("#CompanyName").val();
    var job = $("#JobTitle").val();
    var description = $("#Description").val();
    var start = $('#ExpStartDate').val().trim();
    var end = $('#ExpEndDate').val().trim();
    //var stillWorksHere = $('#StillWorksHere').val().trim();
    console.log("add item... ");
    var endVal = end;
    var stillWorksHere = $('#StillWorksHere').is(":checked")
    if (stillWorksHere) {
        endVal = "Present"
    }


    if ((company && job && start) && (end || stillWorksHere)) {
        var expTableRowCount = document.getElementById("table_exp").getElementsByTagName("tr").length;
        expTableRowCount = expTableRowCount - 1;
        var newExpItem = '<tr style="font-size:12px">' +
            '<td>' + itemCountExp + '</td>' +
            '<td>' + company + ' <input data-val="true" data-val-required="The Company Name field is required." id="WorkExperiences_' + expTableRowCount + '__CompanyName" name="WorkExperiences[' + expTableRowCount + '].CompanyName" type="hidden" value=' + company + '> </td>' +
            '<td>' + job + '  <input data-val="true" data-val-required="The Job Title field is required." id="WorkExperiences_' + expTableRowCount + '__JobTitle" name="WorkExperiences[' + expTableRowCount + '].JobTitle" type="hidden" value=' + job + '> </td>' +
            '<td>' + description + '<input id="WorkExperiences_' + expTableRowCount + '__Description" name="WorkExperiences[' + expTableRowCount + '].Description" type="hidden" value=' + description + '>' + '</td>' +
            '<td>' + start + '<input data-val="true" data-val-date="The field Start Date must be a date." data-val-required="The Start Date field is required." id="WorkExperiences_' + expTableRowCount + '__StartDate" name="WorkExperiences[' + expTableRowCount + '].StartDate" type="hidden" value=' + start + '>' + '</td>' +
            '<td>' + endVal + '<input data-val="true" data-val-date="The field End Date must be a date." data-val-required="The End Date field is required." id="WorkExperiences_' + expTableRowCount + '__EndDate" name="WorkExperiences[' + expTableRowCount + '].EndDate" type="hidden"  value=' + end + '>' + '<input data-val="true" data-val-required="The StillWorksHere field is required." id="WorkExperiences_' + expTableRowCount + '__StillWorksHere" name="WorkExperiences[' + expTableRowCount + '].StillWorksHere" type="hidden" value=' + stillWorksHere + '>' + '</td>' +

            '<td style="width:100px;text-align:center; padding:5px">' + '<button  class="delete btn btn-primary btn-xs my-xs-btn" type="button" id="btnDeleteExp" ">'
+ '<span class="glyphicon glyphicon-trash"></span> Delete </button>' + '</td>';
        createNewItemExp(newExpItem);
        clearExperienceInputFields()
    }

    else {
        alert("Please enter required work experience data");
    }

};

function clearExperienceInputFields() {
    $("#CompanyName").val(""); $("#JobTitle").val(""); $("#Description").val("");
}
function clearEducationInputFields() {
    $("#InstitutionName").val(""); $("#Concentration").val(""); $("#DegreeAwarded").val("");
}
function stillWorksHereChanged(el) {
    var chk = $(el);
    var div = chk.parents("div.col-sm-9")
    var x = div.find("input.form-control").first()
    if (chk.is(":checked")) {
        x.hide();
    }
    else {
        x.show();
    }
}