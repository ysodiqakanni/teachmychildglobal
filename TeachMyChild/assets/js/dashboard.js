function showNavBar() {
    var content = document.getElementById("sidebar");
    var dashboardContent = document.getElementById("main-content");
    if (content.style.display === "none") {
        content.style.display = "block";
        dashboardContent.style.marginLeft = '250px';
    } else {
        content.style.display = "none";
        dashboardContent.style.marginLeft = '0px';
    }
}

function showRightBar() {
    var content = document.getElementById("sidebar-right");

    if (content.style.display === "none") {
        content.style.display = "block";

    } else {
        content.style.display = "none";

    }
}

// graph

var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["June", "July", "August", "September", "October", "November"],
        datasets: [{
            label: 'Spending',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});