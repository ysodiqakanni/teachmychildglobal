﻿debugger
var itemCount = 0;
var dropZone = document.getElementById('drop-zone');
var uploadForm = document.getElementById('js-upload-form');
var docName;
var newItem;
var fileName;

$(function () {
    hideSpinner();
    numberDocumentTable();
    $("#add_doc").click(function (e) {
        addFile();
    })

    $("#upload_profile").click(function (e) {
        console.log("upload clicked")
        
    });

});
function numberDocumentTable() {
    var ind = 1;
    $('#table_upload td:nth-child(1)').each(function () {
        $(this).html(ind++)
    });
}
$(document).on("change", "#register-avatar", function (evt) {
    var tgt = evt.target || window.event.srcElement,
        files = tgt.files;
    if (FileReader && files && files.length) {
        var fr = new FileReader();
        fr.onload = function () {
            $('#profileImage').attr('src', fr.result);
        }
        fr.readAsDataURL(files[0]);
    }
});
$(document).on("click", "#btnDelete", function () {
    if (confirm("Your file wil be removed from the database, continue?")) {
        var profileId = $("#ProfileId").val();
        itemCount--;
        var theRow = $(this).closest("tr");
        var docType = theRow.find(".docType").html();
        // remove from disk also
        var url = "/Tutor/DeleteProfileDocument?profileId=" + profileId + "&docType=" + docType;
        $.ajax({
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            beforeSend: function (){                
                displaySpinner();
            },
            complete: function(){
                hideSpinner();
            },
            error: function () {
                alert("An error occured while deleting the file");
            },
            success: function () {
                alert("File successfuly deleted!");
                theRow.remove();
                numberDocumentTable();
            }
        });
    }
    
});

function displaySpinner() {
    $("#loading").show();
    //$("#loading").html('Processing...' + '<image src="~/assets/img/loading.gif" alt="Loading, please wait" />');
}
function hideSpinner() {
    $("#loading").hide();
    //$("#loading").html('')
}

function createNewItem(file) {

    $('#table_upload').append(file);

};
dropZone.ondrop = function (e) {
    e.preventDefault();
    this.className = 'upload-drop-zone';
    docName = $("#upload").val();
    if (docName == "") {
        alert("Select document name to upload");
        return false;
    }
    if (e.dataTransfer.files.length > 1) {
        alert("Select one file at a time");
        return false;
    }
    var doc = e.dataTransfer.files[0];
    fileName = doc.name;
    if ($('#table_upload td:contains(' + docName + ')').length) {
        alert("Document already exist");
        return false;
    }
    if ($('#table_upload td:contains(' + fileName + ')').length) {
        alert("File name already exist");
        return false;
    }
    itemCount++;

    uploadDoc(docName, fileName, doc);
    
}

dropZone.ondragover = function () {
    this.className = 'upload-drop-zone drop';
    return false;
}

dropZone.ondragleave = function () {
    this.className = 'upload-drop-zone';
    return false;
}

function addFile() {
    //FOR CLARITY, GRAB THE INPUTTED TEXT AND STORE IT IN A VAR
    docName = $("#upload").val();
    if (docName == "") {
        alert("Select document name to upload");
        return false;
    }
    fileName = $("#file_upload").val().replace("C:\\fakepath\\", "");
    if (fileName == "") {
        alert("Browse document to upload");
        return false;
    }
    if ($('#table_upload td:contains(' + docName + ')').length) {
        alert("Document already exists");
        return false;
    }
    if ($('#table_upload td:contains(' + fileName + ')').length) {
        alert("File name already exist");
        return false;
    }
    itemCount++;
    var doc = document.getElementById("file_upload").files[0];
    uploadDoc(docName, fileName, doc);

   


    //$.get(url, function (response) {
    //    var tableData = filesDiv.html();
    //    filesDiv.html(tableData + response);
    //    //filesDiv.append(response);
    //        // Reparse the validator for client side validation
    //        form.data('validator', null);
    //        //$.validator.unobtrusive.parse(form);
    //    });


 //    newItem = '<tr style="font-size:12px">' +
 //                '<td>' + itemCount + '</td>' +
 //                '<td>' + docName + '</td>' +
 //                '<td>' + fileName + '</td>' +
 //                '<td style="width:50px;text-align:center; padding:5px">' + '<button class="delete btn btn-primary btn-xs my-xs-btn glyphicon glyphicon-trash" style="width:60px;" type="button" id="btnDelete" ">'
 //+ '</button>' + '</td>';
 //    createNewItem(newItem);
 //    ClearInputFields();

};

function uploadDoc(docName, fileName, doc) {
    var url = "/Tutor/GetNonVidPartial?docName=" + docName + "&fileName=" + fileName;
    var form = $('form');
    var filesDiv = $('#nonVidFilesDiv');

    var profileId = $("#ProfileId").val();

    
    var formData = new FormData($('form').get(0));
    formData.append("file", doc);
    formData.append("ProfileId", profileId);

    $.ajax({
        type: "POST",
        url: url,
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {          
            displaySpinner();
        },
        complete: function () {
            hideSpinner();
        },
        error: function (e) {
            if (e) {
                alert(e.statusText);
            }
            else {
                alert("An error occured while saving your image");
            }
            
        },
        success: function (response) {
            alert("file saved to database!");
            var tableData = filesDiv.html();
            filesDiv.html(tableData + response);
            numberDocumentTable();
        }
    });

}

function UploadProfileImage() {
    var url = "/Tutor/UploadProfileImage";
    var form = $('form');
    var profileId = $("#ProfileId").val();

    var doc = document.getElementById("register-avatar").files[0];
    if (doc) {
        var formData = new FormData($('form').get(0));
        formData.append("file", doc);
        formData.append("ProfileId", profileId);

        $.ajax({
            type: "POST",
            url: url,
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                displaySpinner();
            },
            complete: function () {
                hideSpinner();
            },
            error: function (e) {
                if (e) {
                    alert(e.statusText);
                }
                else {
                    alert(e.statusText);
                }

            },
            success: function (response) {
                alert("Profile Image Uploaded!");
            }
        });
    }
    else {
        alert("Please select an image file")
    }
  
}

function ClearInputFields() {
    $("#upload").val(""); $("#file_upload").val("");
}


